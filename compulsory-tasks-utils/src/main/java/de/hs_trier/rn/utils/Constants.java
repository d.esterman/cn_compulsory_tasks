/*
 * Constants
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 */
package de.hs_trier.rn.utils;

public interface Constants
{

    public static final String NL = System.getProperty("line.separator");

    public static final String EMPTYSTR = "";
    
    public static final String DOT = ".";
    
    public static final String CSV_EXT = "csv";
    
    public static final String XLSX_EXT = "xlsx";
    
    public static final String JAVA_EXT = "java";
    
    // Symbol to split CSV
    public static final char CSV_SPLIT_BY = ';';

    // Number of digits in matriculation number
    public static final int MAT_NR_LEN = 6;

    // Prompt for GUI-TextField for entering matriculation number
    public static final String PROMPT = "Bitte Matrnr. eingeben";

    // --

    // Column for matriculation number (input file)
    public static final int F_COL_M_NR = 8;

    // Number of columns filled with data (input file)
    public static final int F_COL_NUM = 9;

    // Column for matriculation number (output file)
    public static final int O_COL_M_NR = 0;
    
    // Column for generator polynom (output file)
    public static final int O_COL_G_POL = 1;

    // Column for message polynom - 1st task (output file)
    public static final int O_COL_M_POL_1 = 2;

    // Column for transfered polynom - 1st task (output file)
    public static final int O_COL_T_POL_1 = 3;
    
    // Column for message polynom - 2st task (output file)
    public static final int O_COL_M_POL_2 = 4;
    
    // Column for transfered polynom - 2st task (output file)
    public static final int O_COL_T_POL_2 = 5;

    // Column for exercise (randomly manipulated) polynom (output file)
    public static final int O_COL_E_POL = 6;

    // Column for rest polynom (output file)
    public static final int O_COL_R_POL = 7;
    
    // Column for indication if error is occured polynom (output file)
    public static final int O_COL_IS_ERR = 8;

    // Number of columns for with solutions (CRC output file)
    public static final int CRC_O_COL_NUM_S = 9;

    // Number of columns for output file without solutions (CRC output file)
    public static final int CRC_O_COL_NUM = 4;

    // Generated message polynom mininum length
    public static int M_MIN_LENGTH = 7;

    // Generated message polynom maximum length
    public static int M_MAX_LENGTH = 9;

    // Generated generator polynom mininum length
    public static int G_MIN_LENGTH = 4;

    // Generated generator polynom mininum length
    public static int G_MAX_LENGTH = 5;

    // Addition for file name of sudent's CRC solution
    public static final String STUD_CRC_SOL_FNAME_ADD = "_Abgabe_CRC.txt";

    public static final String QUESTION = "Ist die empfangene Nachricht fehlerbehaftet?";

    public static final String YESNO = "Ja/Nein";

    // --

    // Column for weights of edges for node A
    public static final int O_COL_A_NODE = 1;

    // Column for weights of edges for node B
    public static final int O_COL_B_NODE = 2;

    // Column for weights of edges for node C
    public static final int O_COL_C_NODE = 3;

    // Column for weights of edges for node D
    public static final int O_COL_D_NODE = 4;

    // Column for weights of edges for node E
    public static final int O_COL_E_NODE = 5;

    // Column for solution (LSR output file)
    public static final int LSR_O_COL_SOL = 6;

    // Number of columns for output file with solution (LSR output file)
    public static final int LSR_O_COL_NUM_S = 7;

    // Number of columns for output file without solutions (LSR output file)
    public static final int LSR_O_COL_NUM = 6;

    // Column for student solution (LSR evaluation file)
    public static final int LSR_E_COL_S_SOL = 7;

    // Maximum cost for an edge
    public static final int MAXCOST = 50;

    // Number of nodes
    public static final int NODE_NUMBER = 5;

    // Addition for file name of sudent's LSR solution
    public static final String STUD_LSR_SOL_FNAME_ADD = "_Abgabe_LSR.txt";


    public static String A = "A";

    public static String B = "B";

    public static String C = "C";

    public static String D = "D";

    public static String E = "E";
}
