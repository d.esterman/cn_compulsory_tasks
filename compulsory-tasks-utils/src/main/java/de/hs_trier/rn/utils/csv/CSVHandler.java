/*
 * CSVHandler
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.csv;

import static de.hs_trier.rn.utils.MiscUtils.createRangeArray;
import static de.hs_trier.rn.utils.csv.TableWriter.writeCSV;
import static de.hs_trier.rn.utils.csv.TableWriter.writeXLSX;

import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class CSVHandler
{

    // Singleton: Initialization on Demand Holders
    private static class CRCHolder
    {
        static final CRCCSVHandler CRC_INSTANCE = new CRCCSVHandler();
    }

    private static class LSRHolder
    {
        static final LSRCSVHandler LSR_INSTANCE = new LSRCSVHandler();
    }

    // Instance getters
    public static CRCCSVHandler getCRCInstance()
    {
        return CRCHolder.CRC_INSTANCE;
    }

    public static LSRCSVHandler getLSRInstance()
    {
        return LSRHolder.LSR_INSTANCE;
    }

    public abstract String[] getHeaderWithoutSolution();

    protected abstract String[] getHeaderWithSolution();

    public abstract String[] getHeaderWithUploads();

    public abstract String[][] generateExercises(String[][] inputData);

    public abstract void rewriteSolutions(String string, String[][] readCSV) throws IOException;

    /**
     * Schreibt eine CVS-Datei mit allen Informationen aus dem übergebenen
     * Array: Matrikelnummer, Nachrichtenpolynom, Generatorpolynom, übertragenes
     * Polynom, Aufgabenpolynom (zufällig fehlerbehaftet), Divisionsrest
     * (Musterlösung). Sollte nach Möglichkeit nicht mit dem Tool veröffentlicht
     * werden, sondern geheim gehalten werden :)
     * 
     * @param filePath
     *            Pfad der zu schreibenden Datei
     * @param outputData
     *            Ein zweidimensionales Array mit Matrikelnummern und Aufgaben
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeCSVWithoutSolution(final String filePath, int[] elementIndices, final String[][] outputData) throws FileNotFoundException, IOException
    {
        writeCSV(filePath, getHeaderWithoutSolution(), elementIndices, outputData);
    }

    /**
     * Schreibt eine CVS-Datei mit drei Spalten: Matrikelnummer, Aufgabenpolynom
     * (zufällig fehlerbehaftet), Generatorpolynom. Die geschriebene Datei wird
     * vom veröffentlichten Tool benutzt.
     * 
     * @param filePath
     *            Pfad der zu schreibenden Datei
     * @param outputData
     *            Ein zweidimensionales Array mit Matrikelnummern und Aufgaben
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeXLSXWithSolution(final String filePath, final String[][] outputData) throws FileNotFoundException, IOException
    {
        writeXLSX(filePath, getHeaderWithSolution(), createRangeArray(getHeaderWithSolution().length), outputData);
    }

    public void writeXLSXWithUploads(final String filePath, final String[][] outputData) throws FileNotFoundException, IOException
    {
        writeXLSX(filePath, getHeaderWithUploads(), createRangeArray(getHeaderWithUploads().length), outputData);
    }

}
