/*
 * Dijkstra
 * 
 * Source: http://en.literateprograms.org/Dijkstra%27s_algorithm_%28Java%29
 * 
 */
package de.hs_trier.rn.utils.computing.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class Dijkstra
{
    public static void computePaths(final Vertex source)
    {
        source.minDistance = 0.;
        final PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty())
        {
            final Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (final Edge e : u.adjacencies)
            {
                final Vertex v = e.target;
                final double weight = e.weight;
                final double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance)
                {
                    vertexQueue.remove(v);

                    v.minDistance = distanceThroughU;
                    v.previous = u;
                    vertexQueue.add(v);
                }
            }
        }
    }

    public static List<Vertex> getShortestPathTo(final Vertex target)
    {
        final List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
        {
            path.add(vertex);
        }

        Collections.reverse(path);
        return path;
    }

    public static void main(final String[] args)
    {
        final Vertex a = new Vertex("A");
        final Vertex b = new Vertex("B");
        final Vertex c = new Vertex("C");
        final Vertex d = new Vertex("D");
        final Vertex e = new Vertex("E");

        a.adjacencies = new Edge[]
        { new Edge(b, 10), new Edge(c, 3) };

        b.adjacencies = new Edge[]
        { new Edge(a, 32), new Edge(c, 38), new Edge(d, 37), new Edge(e, 7) };

        c.adjacencies = new Edge[]
        { new Edge(a, 17), new Edge(b, 47), new Edge(d, 1), new Edge(e, 8) };

        d.adjacencies = new Edge[]
        { new Edge(b, 28), new Edge(c, 23), new Edge(e, 37) };

        e.adjacencies = new Edge[]
        { new Edge(b, 46), new Edge(c, 37), new Edge(d, 46) };

        final Vertex[] vertices =
        { b, c, d, e };
        computePaths(a);
        for (final Vertex v : vertices)
        {
            final List<Vertex> path = getShortestPathTo(v);
            Vertex nextHop;
            if (path.size() <= 2)
            {
                nextHop = path.get(0);
            }
            else
            {
                nextHop = path.get(1);
            }
            System.out.println(v + ": " + v.minDistance + " " + nextHop);
        }
    }
}
