package de.hs_trier.rn.utils.csv;

import static de.hs_trier.rn.utils.Constants.CSV_SPLIT_BY;
import static de.hs_trier.rn.utils.MiscUtils.createRangeArray;
import static de.hs_trier.rn.utils.MiscUtils.readLinesFromStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class CSVReader
{
    
    public static String[][] readCSV(InputStream is, int numberOfColumns) throws Exception
    {
        return readCSV(is, createRangeArray(numberOfColumns), false, numberOfColumns);
    }

    public static String[][] readCSV(InputStream is, boolean assigmentOfTarget, int numberOfColumns) throws Exception
    {
        return readCSV(is, createRangeArray(numberOfColumns), assigmentOfTarget, numberOfColumns);
    }

    /**
     * Liest ein CSV-formatierte InputStream ein und erstellt daraus ein
     * zweidimensionales Array. Eine Dimension entspricht der Anzahl der Zeilen,
     * die andere - der Anzahl der Spalten.
     * 
     * @param is
     *            Der zu lesende InputStream
     * @param numberOfColumns
     *            Anzahl der Spalten in CSV
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static String[][] readCSV(InputStream is, int[] assignmentIndices, boolean assigmentOfTarget, int numberOfColumns) throws Exception
    {
        if (assignmentIndices == null)
            throw new Exception("Internal error: CSV could not be read because of missing element indicies.");
    
        String rows[] = readLinesFromStream(is);
        final String result[][] = new String[rows.length - 1][numberOfColumns];
    
        // Hier wird StringBuilder ausgelesen und das Data-Array wird
        // beschrieben
        for (int i = 1; i < rows.length; i++)
        {
            final String columns[] = rows[i].split(String.valueOf(CSV_SPLIT_BY));
            int numberOfColumnsToStore = Math.min(assignmentIndices.length, columns.length);
    
            for (int j = 0; j < numberOfColumnsToStore; j++)
            {
                int index = assignmentIndices[j];
                if (assigmentOfTarget)
                {
                    result[i - 1][j] = columns[index];
                }
                else
                {
                    if (index >= numberOfColumns)
                        throw new Exception("Internal error: Erroneous assignment indicies");
                    result[i - 1][index] = columns[j];
                }
            }
        }
        return result;
    }

    public static String[][] readCSV(String filePath, int numberOfColumns) throws Exception
    {
        return readCSV(filePath, createRangeArray(numberOfColumns), false, numberOfColumns);
    }
    
    public static String[][] readCSV(String filePath, boolean assigmentOfTarget, int numberOfColumns) throws Exception
    {
        return readCSV(filePath, createRangeArray(numberOfColumns), assigmentOfTarget, numberOfColumns);
    }

    /**
     * Liest eine CSV-Datei ein und erstellt daraus ein zweidimensionales Array.
     * Eine Dimension entspricht der Anzahl der Zeilen, die andere - der Anzahl
     * der Spalten.
     * 
     * @param filePath
     *            Pfad der gelesenen CSV-Datei
     * @param numberOfColumns
     *            Anzahl der Spalten in der gelesenen Datei
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static String[][] readCSV(final String filePath, int[] assignmentIndices, boolean assigmentOfTarget, final int numberOfColumns) throws Exception
    {
        try (FileInputStream fis = new FileInputStream(new File(filePath)))
        {
            return readCSV(fis, assignmentIndices, assigmentOfTarget, numberOfColumns);
        }
    }

    public static String[][] readCSV(final String filePath, int[] assignmentIndices, final int numberOfColumns) throws Exception
    {
        return readCSV(filePath, assignmentIndices, false, numberOfColumns);
    }

}
