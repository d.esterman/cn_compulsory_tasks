/*
 * LSRCSVHandler
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.csv;

import static de.hs_trier.rn.utils.Constants.A;
import static de.hs_trier.rn.utils.Constants.B;
import static de.hs_trier.rn.utils.Constants.C;
import static de.hs_trier.rn.utils.Constants.D;
import static de.hs_trier.rn.utils.Constants.E;
import static de.hs_trier.rn.utils.Constants.F_COL_M_NR;
import static de.hs_trier.rn.utils.Constants.LSR_O_COL_NUM_S;
import static de.hs_trier.rn.utils.Constants.LSR_O_COL_SOL;
import static de.hs_trier.rn.utils.Constants.MAXCOST;
import static de.hs_trier.rn.utils.Constants.NODE_NUMBER;
import static de.hs_trier.rn.utils.Constants.O_COL_A_NODE;
import static de.hs_trier.rn.utils.Constants.O_COL_M_NR;
import static de.hs_trier.rn.utils.computing.LSRUtils.others;
import static de.hs_trier.rn.utils.computing.dijkstra.Dijkstra.computePaths;
import static de.hs_trier.rn.utils.computing.dijkstra.Dijkstra.getShortestPathTo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.computing.LSRUtils;
import de.hs_trier.rn.utils.computing.dijkstra.Edge;
import de.hs_trier.rn.utils.computing.dijkstra.Vertex;

public class LSRCSVHandler extends CSVHandler
{
    protected LSRCSVHandler()
    {
    }
    
    @Override
    public String[] getHeaderWithoutSolution()
    {
        return new String[] {"Matrikelnummer", "Knoten A", "Knoten B", "Knoten C", "Knoten D", "Knoten E"};
    }
    
    @Override
    protected String[] getHeaderWithSolution()
    {
        return new String[] {"Matrikelnummer", "Knoten A", "Knoten B", "Knoten C", "Knoten D", "Knoten E", "Lösung"};
    }
    
    @Override
    public String[] getHeaderWithUploads()
    {
        return new String[] {"Matrikelnummer", "Knoten A", "Knoten B", "Knoten C", "Knoten D", "Knoten E", "Lösung", "Lösung vom Student"};
    }

    
    //@formatter:off
    /**
     * Erstellt ein zweidimensionales Array mit folgenden zufällig generierten
     * Werten. <br><ul>
     *   <li> Kosten für Konten A um die Knoten B und C zu erreichen, maximaler Wert 10.<br>
     *   <li> Kosten für Knoten B, C, D und E um sich jeweils gegenseitig zu erreichen, maximaler Wert festgelegt im {@link Constants#MAXCOST}
     * 
     * @param data
     *            Ein zweidimensionales Array, welches mit der Methode {@link CSVHandler#readCSV(String, int)} erstellt wurde.
     * @return Das eingegebene Array, welches mit den Polynomen ergänzt wurde.
     */
     //@formatter:on
    public String[][] generateExercises(String[][] data)
    {
        final String result[][] = new String[data.length][LSR_O_COL_NUM_S];
        for (int i = 0; i < result.length; i++)
        {
            final Vertex[] vertices = new Vertex[]
            { new Vertex(A), new Vertex(B), new Vertex(C), new Vertex(D), new Vertex(E) };
            result[i][O_COL_M_NR] = data[i][F_COL_M_NR];

            final int a_b = LSRUtils.randomInt(10);
            final int a_c = LSRUtils.randomInt(10);
            vertices[0].adjacencies = new Edge[]
            { new Edge(vertices[1], a_b), new Edge(vertices[2], a_c) };
            result[i][O_COL_A_NODE] = a_b + " " + a_c;

            for (int j = 1; j < (NODE_NUMBER); j++)
            {
                final StringBuilder toWrite = new StringBuilder();
                final int[] generatedValuesForNode = new int[NODE_NUMBER - 1];
                for (int k = 0; k < (NODE_NUMBER - 1); k++)
                {
                    generatedValuesForNode[k] = LSRUtils.randomInt(MAXCOST);
                }
                if (j > 2)
                {
                    generatedValuesForNode[0] = 0;
                }
                final Vertex[] others = others(vertices[j], vertices);
                vertices[j].adjacencies = j > 2 ? new Edge[NODE_NUMBER - 2] : new Edge[NODE_NUMBER - 1];
                for (int k = 0, l = j > 2 ? 1 : 0; k < vertices[j].adjacencies.length; k++, l++)
                {
                    vertices[j].adjacencies[k] = new Edge(others[l], generatedValuesForNode[l]);
                }
                for (int k = 0; k < generatedValuesForNode.length; k++)
                {
                    toWrite.append(generatedValuesForNode[k]);
                    if (k < (NODE_NUMBER - 2))
                    {
                        toWrite.append(" ");
                    }
                }
                result[i][O_COL_A_NODE + j] = toWrite.toString();
            }

            computePaths(vertices[0]);
            final StringBuilder sb = new StringBuilder();
            for (int j = 0; j < (NODE_NUMBER - 1); j++)
            {
                final List<Vertex> path = getShortestPathTo(vertices[j + 1]);
                sb.append((int) vertices[j + 1].minDistance + " " + path.get(1) + " ");
            }
            result[i][LSR_O_COL_SOL] = sb.toString().trim();
        }
        return result;
    }

    /**
     * Liest die Datei mit LSR-Aufgaben und berechnet die Musterlösungen neu. 
     * 
     * @throws FileNotFoundException
     * 
     * @throws IOException
     */
    public void rewriteSolutions(String filePath, String[][] data) throws IOException
    {
        final String result[][] = new String[data.length][LSR_O_COL_NUM_S];
        for (int i = 0; i < result.length; i++)
        {
            result[i] = Arrays.copyOf(data[i], LSR_O_COL_NUM_S);
        }
        for (int i = 0; i < data.length; i++)
        {
            final Vertex[] vertices =
            { new Vertex(A), new Vertex(B), new Vertex(C), new Vertex(D), new Vertex(E) };
            final String[] valuesA = data[i][O_COL_A_NODE].split(" ");
            vertices[0].adjacencies = new Edge[]
            { new Edge(vertices[1], Integer.parseInt(valuesA[0])), new Edge(vertices[2], Integer.parseInt(valuesA[1])) };
            for (int j = 1; j <= 4; j++)
            {
                final String[] values = data[i][O_COL_A_NODE + j].split(" ");
                final Vertex[] others = others(vertices[j], vertices);
                vertices[j].adjacencies = new Edge[4];
                for (int k = 0; k < others.length; k++)
                {
                    vertices[j].adjacencies[k] = new Edge(others[k], Integer.parseInt(values[k]));
                }
            }
            computePaths(vertices[0]);
            final StringBuilder sb = new StringBuilder();
            for (int j = 0; j < (NODE_NUMBER - 1); j++)
            {
                final List<Vertex> path = getShortestPathTo(vertices[j + 1]);
                sb.append((int) vertices[j + 1].minDistance + " " + path.get(1) + " ");
            }
            result[i][LSR_O_COL_SOL] = sb.toString().trim();
        }
        writeXLSXWithSolution(filePath, result);
    }
}
