/*
 * Edge
 * 
 * Source: http://en.literateprograms.org/Dijkstra%27s_algorithm_%28Java%29
 * 
 */
package de.hs_trier.rn.utils.computing.dijkstra;

public class Edge
{
    final Vertex target;

    final double weight;

    public Edge(final Vertex argTarget, final double argWeight)
    {
        target = argTarget;
        weight = argWeight;
    }
}
