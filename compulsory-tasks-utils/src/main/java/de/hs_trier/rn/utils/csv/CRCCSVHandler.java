/*
 * CRCCSVHandler
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.csv;

import static de.hs_trier.rn.utils.Constants.CRC_O_COL_NUM_S;
import static de.hs_trier.rn.utils.Constants.F_COL_M_NR;
import static de.hs_trier.rn.utils.Constants.G_MAX_LENGTH;
import static de.hs_trier.rn.utils.Constants.G_MIN_LENGTH;
import static de.hs_trier.rn.utils.Constants.M_MAX_LENGTH;
import static de.hs_trier.rn.utils.Constants.M_MIN_LENGTH;
import static de.hs_trier.rn.utils.Constants.O_COL_E_POL;
import static de.hs_trier.rn.utils.Constants.O_COL_G_POL;
import static de.hs_trier.rn.utils.Constants.O_COL_IS_ERR;
import static de.hs_trier.rn.utils.Constants.O_COL_M_NR;
import static de.hs_trier.rn.utils.Constants.O_COL_M_POL_1;
import static de.hs_trier.rn.utils.Constants.O_COL_M_POL_2;
import static de.hs_trier.rn.utils.Constants.O_COL_R_POL;
import static de.hs_trier.rn.utils.Constants.O_COL_T_POL_1;
import static de.hs_trier.rn.utils.Constants.O_COL_T_POL_2;

import java.io.IOException;
import java.util.Arrays;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.computing.CRCUtils;

public class CRCCSVHandler extends CSVHandler
{

    @Override
    protected String[] getHeaderWithSolution()
    {
        return new String[]
        { "Mnr", "Generator", "Nachricht1", "Nachricht1+CRC", "Nachricht2", "Nachricht2+CRC", "Übertragung", "Divisionsrest", "Fehler?" };
    }

    @Override
    public String[] getHeaderWithoutSolution()
    {
        return new String[]
        { "Mnr", "Generator", "Nachricht1", "Übertragung" };
    }

    @Override
    public String[] getHeaderWithUploads()
    {
        return new String[]
        { "Mnr", "Generator", "Nachricht1", "Nachricht1+CRC", "Nachricht2", "Nachricht2+CRC", "Übertragung", "Divisionsrest", "Fehler?", "Student_Nachricht1+CRC", "Student_Divisionsrest", "Student_Fehler?", "Korrekt1", "Korrekt2", "Korrekt3", "Ges.Korrektheit" };
    }

    //@formatter:off
    /**
     * Erstellt ein zweidimensionales Array mit folgenden zufällig generierten
     * Werten. 
     *   - generatedMessagePolynom: Ein Nachrichtenpolynom
     *   - generatedGeneratorPolynom: Ein Generatorpolynom 
     *   - transferedPolynom: Ein Nachrichtenpolynom inklusive redundanten Bits
     *   - exercisePolynom: Ein Nachrichtenpolynom für die Aufgabe, wird zufällig mit Fehlern behaftet
     *   - divisionRest: Divisionsrest des Aufgabenpolynoms durch das Generatorpolynom, Musterlösung
     * 
     * @param data
     *            Ein zweidimensionales Array, welches mit der Methode
     *            readCVS erstellt wurde.
     * @return Das eingegebene Array, welches mit den Polynomen ergänzt wurde.
     */
     //@formatter:on
    public String[][] generateExercises(String[][] data)
    {
        final String result[][] = new String[data.length][CRC_O_COL_NUM_S];
        for (int i = 0; i < result.length; i++)
        {
            final int generatorPolynomLength = G_MIN_LENGTH + (int) (Math.random() * ((G_MAX_LENGTH - G_MIN_LENGTH) + 1));
            final int messagePolynom1Length = M_MIN_LENGTH + (int) (Math.random() * ((M_MAX_LENGTH - M_MIN_LENGTH) + 1));
            final int messagePolynom2Length = M_MIN_LENGTH + (int) (Math.random() * ((M_MAX_LENGTH - M_MIN_LENGTH) + 1));

            final char[] generatorPolynomArray = new char[generatorPolynomLength];
            final char[] messagePolynom1Array = new char[messagePolynom1Length];
            final char[] messagePolynom2Array = new char[messagePolynom2Length];

            // Das erste Bit soll immer gesetzt sein
            generatorPolynomArray[0] = '1';
            messagePolynom1Array[0] = '1';
            messagePolynom2Array[0] = '1';

            for (int j = 1; j < generatorPolynomLength; j++)
                generatorPolynomArray[j] = CRCUtils.diceZeroOrOne();

            for (int j = 1; j < messagePolynom1Length; j++)
                messagePolynom1Array[j] = CRCUtils.diceZeroOrOne();

            for (int j = 1; j < messagePolynom2Length; j++)
                messagePolynom2Array[j] = CRCUtils.diceZeroOrOne();

            // Vermeidung von N1=N2
            while (Arrays.equals(messagePolynom1Array, messagePolynom2Array))
            {
                for (int j = 1; j < messagePolynom2Length; j++)
                    messagePolynom2Array[j] = CRCUtils.diceZeroOrOne();
            }

            final String generatorPolynom = new String(generatorPolynomArray);
            final String messagePolynom1 = new String(messagePolynom1Array);
            final String transferedPolynom1 = CRCUtils.computeTransferredPolynom(messagePolynom1, generatorPolynom);
            final String messagePolynom2 = new String(messagePolynom2Array);
            final String transferedPolynom2 = CRCUtils.computeTransferredPolynom(messagePolynom2, generatorPolynom);
            final String exercisePolynom = CRCUtils.manipulatePolynom(transferedPolynom2);
            final String divisionRest = MiscUtils.removeLeadingZeros(CRCUtils.computeRest(exercisePolynom, generatorPolynom));
            final String isError = "0".equals(divisionRest) ? "=false" : "=true";

            result[i][O_COL_M_NR] = data[i][F_COL_M_NR];
            result[i][O_COL_G_POL] = generatorPolynom;
            result[i][O_COL_M_POL_1] = messagePolynom1;
            result[i][O_COL_T_POL_1] = transferedPolynom1;
            result[i][O_COL_M_POL_2] = messagePolynom2;
            result[i][O_COL_T_POL_2] = transferedPolynom2;
            result[i][O_COL_E_POL] = exercisePolynom;
            result[i][O_COL_R_POL] = divisionRest;
            result[i][O_COL_IS_ERR] = isError;
        }
        return result;
    }

    /**
     * Liest die Datei mit CRC-Aufgaben und berechnet die Musterlösungen neu.
     * (Wurde ursprünglich benutzt beim Debuggen der Klasse {@link CRCUtils})
     * 
     * @throws IOException
     */
    public void rewriteSolutions(String filePath, String[][] data) throws IOException
    {
        for (int i = 0; i < data.length; i++)
        {
            data[i][O_COL_T_POL_1] = CRCUtils.computeTransferredPolynom(data[i][Constants.O_COL_M_POL_1], data[i][Constants.O_COL_G_POL]);
            data[i][O_COL_R_POL] = MiscUtils.removeLeadingZeros(CRCUtils.computeRest(data[i][Constants.O_COL_E_POL], data[i][Constants.O_COL_G_POL]));
            data[i][O_COL_IS_ERR] = "0".equals(data[i][O_COL_R_POL]) ? "=false" : "=true";
        }
        writeXLSXWithSolution(filePath, data);
    }

}
