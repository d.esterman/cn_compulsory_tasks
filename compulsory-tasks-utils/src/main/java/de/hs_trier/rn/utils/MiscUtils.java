package de.hs_trier.rn.utils;

import static de.hs_trier.rn.utils.Constants.NL;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MiscUtils
{

    public static int[] createRangeArray(int range)
    {
        int[] indices = new int[range];
        for (int i = 0; i < indices.length; i++)
        {
            indices[i] = i;
        }
        return indices;
    }

    public static String nvl(String str)
    {
        return str == null ? Constants.EMPTYSTR : str;
    }

    public static String assembleFileName(String directory, String fileName, String addition, String extension)
    {
        String fileNamePart = fileName.substring(fileName.length() - extension.length()).equalsIgnoreCase(extension) && fileName.charAt(fileName.length() - extension.length() - 1) == Constants.DOT.charAt(0) ? fileName.substring(0, fileName.length() - extension.length() - 1) : fileName;

        if (directory == null)
            return fileNamePart + addition + Constants.DOT + extension;
        else
            return directory + File.separator + fileNamePart + addition + Constants.DOT + extension;
    }

    public static String assembleFileName(String directory, String fileName, String extension)
    {
        return assembleFileName(directory, fileName, Constants.EMPTYSTR, extension);
    }

    public static StringBuffer readStreamIntoStringBuffer(InputStream is) throws IOException
    {
        String line = Constants.EMPTYSTR;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is)))
        {
            StringBuffer sb = new StringBuffer();
            while ((line = br.readLine()) != null)
            {
                sb.append(line).append(NL);
            }
            sb.delete(sb.length() - NL.length(), sb.length());
            return sb;
        }
    }
    
    public static String[] readLinesFromStream(InputStream is) throws IOException
    {
        return readStreamIntoStringBuffer(is).toString().split("(\r\n)|(\r)|(\n)");
    }
    
    public static String readStreamIntoLine(InputStream is) throws IOException
    {
        return readStreamIntoStringBuffer(is).toString().replaceAll("\r\n|\r|\n", " ");
    }

    // Source: http://codereview.stackexchange.com/q/44545/74350
    public static String toExcelColumnName(int number)
    {
        StringBuilder sb = new StringBuilder();
        while (number-- > 0)
        {
            sb.append((char) ('A' + (number % 26)));
            number /= 26;
        }
        return sb.reverse().toString();
    }

    public static int convertBoolToInt(final boolean b)
    {
        return b ? 1 : 0;
    }

    public static boolean convertNumericStringToBool(final String s)
    {
        try
        {
            return Integer.parseInt(s) == 1;
        }
        catch (NumberFormatException nfe)
        {
            return false;
        }
    }

    public static String removeLeadingZeros(String s)
    {
        return s.replaceFirst("^0+(?!$)", "");
    }
}
