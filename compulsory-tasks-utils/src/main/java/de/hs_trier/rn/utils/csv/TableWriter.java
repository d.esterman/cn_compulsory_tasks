package de.hs_trier.rn.utils.csv;

import static de.hs_trier.rn.utils.Constants.CSV_SPLIT_BY;
import static de.hs_trier.rn.utils.Constants.NL;
import static de.hs_trier.rn.utils.MiscUtils.nvl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;

public class TableWriter
{

    public static void writeCSV(final String filePath, String[] headers, int[] assignmentIndices, final String[][] outputData) throws FileNotFoundException, IOException
    {
        prepareFolder(filePath);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath)))
        {
            for (String header : headers)
            {
                bw.write(header);
                bw.write(Constants.CSV_SPLIT_BY);
            }
            bw.write(NL);
            for (String[] element : outputData)
            {
                for (int i = 0; i < headers.length; i++)
                {
                    bw.write(nvl(element[assignmentIndices[i]]));
                    bw.write(CSV_SPLIT_BY);
                }
                bw.write(NL);
            }
        }
    }
    
    public static void writeXLSX(final String filePath, String[] headers, final String[][] outputData) throws FileNotFoundException, IOException
    {
        writeXLSX(filePath, headers, MiscUtils.createRangeArray(headers.length), outputData);
    }

    public static void writeXLSX(final String filePath, String[] headers, int[] assignmentIndices, final String[][] outputData) throws FileNotFoundException, IOException
    {
        prepareFolder(filePath);
        try (FileOutputStream fos = new FileOutputStream(filePath, false);
        SXSSFWorkbook wb = new SXSSFWorkbook(outputData.length + 1))
        {
            CellStyle normalStyle = wb.createCellStyle();
            setBorders(normalStyle, CellStyle.BORDER_THIN);

            Sheet sh = wb.createSheet();
            ((SXSSFSheet) sh).trackAllColumnsForAutoSizing();
            Row titleRow = sh.createRow(0);
            XSSFCellStyle titleRowStyle = (XSSFCellStyle) wb.createCellStyle();
            XSSFFont boldFont = (XSSFFont) wb.createFont();
            boldFont.setBold(true);
            titleRowStyle.setFont(boldFont);
            setBorders(titleRowStyle, CellStyle.BORDER_THICK);

            for (int i = 0; i < headers.length; i++)
            {
                Cell c = titleRow.createCell(i);
                c.setCellValue(headers[i]);
                c.setCellStyle(titleRowStyle);
            }
            for (int i = 0; i < outputData.length; i++)
            {
                Row r = sh.createRow(i + 1);
                String[] element = outputData[i];
                for (int j = 0; j < headers.length; j++)
                {
                    Cell c = r.createCell(j);
                    String value = element[assignmentIndices[j]];
                    if (value != null)
                        if (value.startsWith("=") && value.length() > 1)
                        {
                            c.setCellType(Cell.CELL_TYPE_FORMULA);
                            c.setCellFormula(value.substring(1));
                        }
                        else
                            c.setCellValue(value);
                    c.setCellStyle(normalStyle);
                }
            }

            sh.setAutoFilter(new CellRangeAddress(0, 0, 0, headers.length - 1));
            for (int j = 0; j < headers.length; j++)
            {
                sh.autoSizeColumn(j);
                sh.setColumnWidth(j, sh.getColumnWidth(j) + 511);
            }
            sh.createFreezePane(0, 1);
            wb.write(fos);
        }
    }

    public static void setBorders(CellStyle style, short border)
    {
        style.setBorderLeft(border);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(border);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderRight(border);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(border);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    }

    public static void prepareFolder(final String filePath) throws IOException
    {
        File parentFolder = (new File(filePath).getAbsoluteFile()).getParentFile();
        if (!parentFolder.exists())
        {
            if (!parentFolder.mkdirs())
                throw new IOException("Path creation failed (" + parentFolder.getAbsolutePath() + ")");
        }
    }

}
