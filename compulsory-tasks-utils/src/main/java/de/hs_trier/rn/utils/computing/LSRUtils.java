/*
 * LSRUtils
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.computing;

import static de.hs_trier.rn.utils.Constants.NODE_NUMBER;

import java.util.ArrayList;
import java.util.List;

import de.hs_trier.rn.utils.computing.dijkstra.Vertex;

public class LSRUtils
{
    public static int randomInt(final int i)
    {
        return (int) ((Math.random() * i) + 1);
    }

    public static int randomNodeNumber()
    {
        return ((int) ((Math.random() * (NODE_NUMBER - 1))));
    }

    public static Vertex[] others(final Vertex v, final Vertex[] vertices)
    {
        final Vertex[] toRet = new Vertex[vertices.length - 1];
        final List<Vertex> others = new ArrayList<Vertex>();
        for (final Vertex vertex : vertices)
        {
            if (!vertex.equals(v))
            {
                others.add(vertex);
            }
        }

        others.toArray(toRet);

        return toRet;
    }
}
