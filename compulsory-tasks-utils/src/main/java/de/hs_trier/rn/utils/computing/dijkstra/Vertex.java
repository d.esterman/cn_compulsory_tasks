/*
 * Vertex
 * 
 * Source: http://en.literateprograms.org/Dijkstra%27s_algorithm_%28Java%29
 * 
 */
package de.hs_trier.rn.utils.computing.dijkstra;

public class Vertex implements Comparable<Vertex>
{
    private final String name;

    public Edge[] adjacencies;

    public double minDistance = Double.POSITIVE_INFINITY;

    Vertex previous;

    public Vertex(final String argName)
    {
        name = argName;
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public int compareTo(final Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }

}
