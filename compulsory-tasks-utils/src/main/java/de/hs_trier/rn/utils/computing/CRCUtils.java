/*
 * CRCUtils
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.computing;

import java.util.Arrays;
import java.util.Random;

public class CRCUtils
{
    private static int xor(final int x, final int y)
    {
        if (x == y)
        {
            return (0);
        }
        else
        {
            return (1);
        }
    }

    public static String computeRest(final String m, final String g)
    {
        int gLen, mLen, msb, i, j, k;
        int z[], r[];
        mLen = m.length();
        gLen = g.length();

        final int mBits[] = new int[mLen];
        for (i = 0; i < mBits.length; i++)
        {
            mBits[i] = Integer.parseInt(String.valueOf(m.charAt(i)));
        }
        final int gBits[] = new int[gLen];
        for (i = 0; i < gBits.length; i++)
        {
            gBits[i] = Integer.parseInt(String.valueOf(g.charAt(i)));
        }
        r = new int[mLen];
        for (i = 0; i < gLen; i++)
        {
            r[i] = mBits[i];
        }
        z = new int[gLen];
        for (i = 0; i < gLen; i++)
        {
            z[i] = 0;
        }
        for (i = 0; i <= (mLen - gLen); i++)
        {
            k = 0;
            msb = r[i];
            for (j = i; j < (gLen + i); j++)
            {
                if (msb == 0)
                {
                    r[j] = xor(r[j], z[k]);
                }
                else
                {
                    r[j] = xor(r[j], gBits[k]);
                }
                k++;
            }
            if (i < (mLen - gLen))
            {
                r[gLen + i] = mBits[gLen + i];
            }
        }
        final String resultString = Arrays.toString(r);
        return resultString.substring(1, resultString.length() - 1).replace(", ", "");
    }

    public static String computeCRC(final String m, final String g)
    {
        final int appendedZeroes = g.length() - 1;
        return computeRest(String.format("%s%0" + appendedZeroes + "d", m, 0), g).substring(m.length());
    }

    public static String computeTransferredPolynom(final String m, final String g)
    {
        return m.concat(computeCRC(m, g));
    }

    public static char diceZeroOrOne()
    {
        if (Math.random() < 0.5)
        {
            return '0';
        }
        else
        {
            return '1';
        }
    }

    public static String manipulatePolynom(final String transferedPolynom)
    {
        final int randomPos = new Random().nextInt(transferedPolynom.length() - 1) + 1;
        return transferedPolynom.substring(0, randomPos) + diceZeroOrOne() + transferedPolynom.substring(randomPos + 1);
    }
}
