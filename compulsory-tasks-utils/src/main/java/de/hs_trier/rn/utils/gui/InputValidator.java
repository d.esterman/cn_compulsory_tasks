/*
 * InputValidator
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.utils.gui;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultHighlighter;

import de.hs_trier.rn.utils.Constants;


public final class InputValidator extends KeyAdapter
{
    private final JTextField matNrTextField;

    private final JButton loadButton;

    public InputValidator(final JTextField matNrTextField, final JButton loadButton)
    {
        this.matNrTextField = matNrTextField;
        this.loadButton = loadButton;
    }

    @Override
    public void keyTyped(final KeyEvent e)
    {
        final int prevCarretPos;
        final boolean selectionActiveAndCursorIsOnTheRight = matNrTextField.getSelectionStart() < matNrTextField.getCaretPosition();
        if (selectionActiveAndCursorIsOnTheRight)
        {
            prevCarretPos = matNrTextField.getSelectionStart();
        }
        else
        {
            prevCarretPos = matNrTextField.getCaretPosition();
        }
        SwingUtilities.invokeLater(new Runnable()
        {

            @Override
            public void run()
            {
                String currInput = matNrTextField.getText().replaceAll("\\D", ""); // only
                                                                                   // digits
                                                                                   // allowed
                // we have a valid input
                if ((currInput.length() != 0))
                {
                    // maximum allowed digits: MAT_NR_LEN
                    if (currInput.length() > Constants.MAT_NR_LEN)
                    {
                        currInput = currInput.substring(0, Constants.MAT_NR_LEN);
                    }

                    // enable button only if 6 digits entered
                    if (currInput.length() < Constants.MAT_NR_LEN)
                    {
                        loadButton.setEnabled(false);
                    }
                    else
                    {
                        loadButton.setEnabled(true);
                    }

                    // the most important: set valid input into text field
                    matNrTextField.setText(currInput);
                    matNrTextField.setForeground(Color.BLACK);
                    matNrTextField.setHighlighter(new DefaultHighlighter());

                    // set caret to right position
                    if (prevCarretPos < currInput.length())
                    {
                        if ((e.getKeyChar() >= KeyEvent.VK_0) && (e.getKeyChar() <= KeyEvent.VK_9) && (prevCarretPos < Constants.MAT_NR_LEN))
                        {
                            matNrTextField.setCaretPosition(prevCarretPos + 1);
                        }
                        else
                        {
                            matNrTextField.setCaretPosition(prevCarretPos);
                        }
                    }
                }

                // no valid input - show prompt
                else
                {
                    matNrTextField.setText(Constants.PROMPT);
                    matNrTextField.setForeground(Color.GRAY);
                    matNrTextField.setCaretPosition(0);
                    matNrTextField.setHighlighter(null);
                }
            }
        });
    }
}