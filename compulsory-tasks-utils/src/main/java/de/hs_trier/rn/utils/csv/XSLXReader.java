package de.hs_trier.rn.utils.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XSLXReader
{
    public static String[][] readXLSX(InputStream is, int numberOfColumns) throws IOException
    {
        try (XSSFWorkbook wb = new XSSFWorkbook(is))
        {
            Sheet sh = wb.getSheetAt(0);
            int numberOfRows = sh.getLastRowNum();
            String[][] data = new String[numberOfRows][numberOfColumns];
            FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
            for (int i = 0; i < numberOfRows; i++)
            {
                Row r = sh.getRow(i + 1);
                int numberOfColumnsToStore = Math.min(numberOfColumns, r.getLastCellNum());
                for (int j = 0; j < numberOfColumnsToStore; j++)
                {
                    Cell c = r.getCell(j);
                    CellValue cv = evaluator.evaluate(c);
                    int cellType = cv.getCellType();
                    String value = null;
                    switch (cellType)
                    {
                        case Cell.CELL_TYPE_STRING:
                            value = cv.getStringValue();
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            value = "="+cv.getBooleanValue();
                            break;
                    }
                    data[i][j] = value;
                }
            }
            return data;
        }
    }

    public static String[][] readXLSX(String filePath, int numberOfColumns) throws IOException
    {
        try (FileInputStream fis = new FileInputStream(new File(filePath)))
        {
            return readXLSX(fis, numberOfColumns);
        }
    }
}
