/*
 * LSRExercise
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.gui.lsr;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.csv.CSVReader;
import de.hs_trier.rn.utils.gui.InputValidator;

public class LSRExercise extends JFrame
{
    abstract class AbstractDocumentFilter extends DocumentFilter
    {
        private final int limit;

        AbstractDocumentFilter(final int limit)
        {
            super();
            this.limit = limit;
        }

        abstract protected boolean check(String str, int pos);

        @Override
        public void insertString(final DocumentFilter.FilterBypass fp, final int offset, final String string, final AttributeSet aset) throws BadLocationException
        {
            final String upperCase = string.toUpperCase();
            final int len = upperCase.length();
            boolean isValidInput = true;

            for (int i = 0; i < len; i++)
            {
                if (check(upperCase, i))
                {
                    isValidInput = false;
                    break;
                }
            }
            if (isValidInput && ((fp.getDocument().getLength() + upperCase.length()) <= limit))
            {
                super.insertString(fp, offset, upperCase, aset);
            }
            else
            {
                Toolkit.getDefaultToolkit().beep();
            }
        }

        @Override
        public void replace(final DocumentFilter.FilterBypass fp, final int offset, final int length, final String string, final AttributeSet aset) throws BadLocationException
        {
            final String upperCase = string.toUpperCase();
            final int len = upperCase.length();
            boolean isValidInput = true;

            for (int i = 0; i < len; i++)
            {
                if (check(upperCase, i))
                {
                    isValidInput = false;
                    break;
                }
            }
            if (isValidInput && ((fp.getDocument().getLength() + upperCase.length()) <= limit))
            {
                super.replace(fp, offset, length, upperCase, aset);
            }
        }
    }

    class DigitDocumentFilter extends AbstractDocumentFilter
    {
        DigitDocumentFilter(final int limit)
        {
            super(limit);
        }

        @Override
        protected boolean check(final String str, final int pos)
        {
            return !Character.isDigit(str.charAt(pos));
        }
    }

    class AtoEUpperCaseDocumentFilter extends AbstractDocumentFilter
    {
        AtoEUpperCaseDocumentFilter(final int limit)
        {
            super(limit);
        }

        @Override
        protected boolean check(final String str, final int pos)
        {
            return !((str.charAt(pos) == 'A') || (str.charAt(pos) == 'B') || (str.charAt(pos) == 'C') || (str.charAt(pos) == 'D') || (str.charAt(pos) == 'E'));
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = -306888616716275814L;

    private static final String INPUT_FILE = "lsr_data.csv";

    private String[][] data;

    private static Map<String, Integer> matrikelNummern;

    private Integer indexOfMatNr;

    private JButton loadButton;

    private JButton browseButton;

    private JButton saveButton;

    private JTextField locationTextField;

    private final String lineSeparator = System.getProperty("line.separator");

    private final String filler = "     ";

    private final Color backgroundColor;

    private JPanel nodeBPanel;

    private JLabel lblB_1;

    private JSeparator separator_5;

    private JLabel lblA_1;

    private JSeparator separator_6;

    private JTextPane textPaneB_A;

    private JSeparator separator_7;

    private JLabel lblB_2;

    private JTextPane textPaneB_C;

    private JSeparator separator_8;

    private JLabel lblC_2;

    private JTextPane textPaneB_D;

    private JSeparator separator_9;

    private JLabel lblD_2;

    private JTextPane textPaneB_E;

    private JPanel nodeCPanel;

    private JLabel lblC_1;

    private JSeparator separator_10;

    private JLabel lblA_2;

    private JSeparator separator_11;

    private JTextPane textAreaC_A;

    private JSeparator separator_12;

    private JLabel lblB_3;

    private JTextPane textPaneC_B;

    private JSeparator separator_13;

    private JLabel label_8;

    private JTextPane textPaneC_D;

    private JSeparator separator_14;

    private JLabel label_9;

    private JTextPane textPaneC_E;

    private JPanel nodeDPanel;

    private JLabel lblD_1;

    private JSeparator separator_15;

    private JLabel lblA_3;

    private JSeparator separator_16;

    private JTextPane textPaneD_A;

    private JSeparator separator_17;

    private JLabel lblB_4;

    private JTextPane textPaneD_B;

    private JSeparator separator_18;

    private JLabel lblC_3;

    private JTextPane textPaneD_C;

    private JSeparator separator_19;

    private JLabel label_14;

    private JTextPane textPaneD_E;

    private JPanel nodeEPanel;

    private JLabel lblE_1;

    private JSeparator separator_20;

    private JLabel lblA_4;

    private JSeparator separator_21;

    private JTextPane textPaneE_A;

    private JSeparator separator_22;

    private JLabel lblB_5;

    private JTextPane textPaneE_B;

    private JSeparator separator_23;

    private JLabel lblC_4;

    private JTextPane textPaneE_C;

    private JSeparator separator_24;

    private JLabel lblD_3;

    private JTextPane textPaneE_D;

    private JSeparator separator;

    private JPanel sPanel;

    private JPanel sInnerPanel;

    private JLabel lblTargetNode;

    private JLabel targetNodeLabelB;

    private JLabel targetNodeLabelC;

    private JLabel targetNodeLabelD;

    private JLabel targetNodeLabelE;

    private JLabel lblCost;

    private JTextField costTextField1;

    private JTextField costTextField2;

    private JTextField costTextField3;

    private JTextField costTextField4;

    private JLabel lblNextHop;

    private JTextField nextHopTextField1;

    private JTextField nextHopTextField2;

    private JTextField nextHopTextField3;

    private JTextField nextHopTextField4;

    private JSeparator separator_1;

    private JSeparator separator_2;

    private JSeparator separator_3;

    private JSeparator separator_4;

    private JSeparator separator_25;

    private JSeparator separator_26;

    private JSeparator separator_27;

    private JSeparator separator_28;

    private JSeparator separator_29;

    private JSeparator separator_30;

    private JLabel label;

    private JPanel panel;

    private JLabel lblLinkStatePakete;

    private StyledDocument textPaneDocument;

    private JLabel lblNeighbours;

    private JTextPane neighbourNodesPane;

    private final String neighbourPaneString = "(A, B) = %s; (A, C) = %s";

    private String a_b = "  ", a_c = "  ";

    public LSRExercise(final String title)
    {
        super(title);

        try
        {
            data = CSVReader.readCSV(getClass().getClassLoader().getResourceAsStream(INPUT_FILE), Constants.LSR_O_COL_NUM);
        }
        catch (final Exception e)
        {
            JOptionPane.showMessageDialog(null, "Die Aufgabendaten konnten nicht geladen werden.", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        matrikelNummern = new HashMap<>();
        for (int i = 0; i < data.length; i++)
        {
            matrikelNummern.put(data[i][Constants.O_COL_M_NR], i);
        }

        // Build GUI
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // Prepare Layouts
        final BoxLayout bLayout = new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS);

        final Insets horizVertInsets = new Insets(5, 5, 5, 5);
        final Insets solCompInsets = new Insets(3, 0, 3, 0);
        final Insets solTitleInsets = new Insets(0, 0, 5, 0);
        final Insets nullInsets = new Insets(0, 0, 0, 0);
        final Insets buttonMarginInsets = new Insets(-1, 0, -2, 0);

        final SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);

        getContentPane().setLayout(bLayout);
        backgroundColor = getContentPane().getBackground();

        // Exercise loading panel
        final JPanel eLoadPanel = new JPanel();
        final GridBagLayout gbLayout_eLoadPanel = new GridBagLayout();
        eLoadPanel.setLayout(gbLayout_eLoadPanel);

        final JTextField matNrTextField = new JTextField(Constants.PROMPT);
        matNrTextField.setForeground(Color.GRAY);
        matNrTextField.setHighlighter(null);
        final GridBagConstraints gbc_matNrTextField = new GridBagConstraints();
        gbc_matNrTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_matNrTextField.gridx = 0;
        gbc_matNrTextField.gridy = 0;
        gbc_matNrTextField.weightx = 1;
        gbc_matNrTextField.insets = new Insets(0, 210, 0, 0);
        eLoadPanel.add(matNrTextField, gbc_matNrTextField);

        loadButton = new JButton("Aufgabe laden");
        loadButton.setEnabled(false);
        final GridBagConstraints gbc_loadButton = new GridBagConstraints();
        gbc_loadButton.fill = GridBagConstraints.NONE;
        gbc_loadButton.gridx = 1;
        gbc_loadButton.gridy = 0;
        gbc_loadButton.weightx = 0;
        gbc_loadButton.weighty = 0;
        gbc_loadButton.insets = horizVertInsets;
        loadButton.setMargin(buttonMarginInsets);
        eLoadPanel.add(loadButton, gbc_loadButton);

        getContentPane().add(eLoadPanel);

        getContentPane().add(new JSeparator(SwingConstants.HORIZONTAL));

        // Questions panel
        final JPanel ePanel = new JPanel();

        getContentPane().add(ePanel);
        final GridBagLayout gbl_ePanel = new GridBagLayout();
        gbl_ePanel.columnWidths = new int[]
        { 0 };
        gbl_ePanel.rowHeights = new int[]
        { 0, 0, 0, 0 };
        gbl_ePanel.columnWeights = new double[]
        { 1.0 };
        gbl_ePanel.rowWeights = new double[]
        { 0.0, 1.0, 0.0 };
        ePanel.setLayout(gbl_ePanel);

        lblNeighbours = new JLabel("Vom Knoten A gemessene Kosten zu den Nachbarknoten:");
        lblNeighbours.setFont(new Font("Tahoma", Font.BOLD, 13));
        final GridBagConstraints gbc_lblNeighbours = new GridBagConstraints();
        gbc_lblNeighbours.insets = new Insets(0, 0, 5, 0);
        gbc_lblNeighbours.gridx = 0;
        gbc_lblNeighbours.gridy = 0;
        ePanel.add(lblNeighbours, gbc_lblNeighbours);

        neighbourNodesPane = new JTextPane();
        neighbourNodesPane.setText(formatNeighbourString());
        textPaneDocument = neighbourNodesPane.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        neighbourNodesPane.setFocusable(false);
        neighbourNodesPane.setEditable(false);
        neighbourNodesPane.setBackground(backgroundColor);
        final GridBagConstraints gbc_textPane = new GridBagConstraints();
        gbc_textPane.insets = new Insets(0, 0, 5, 0);
        gbc_textPane.fill = GridBagConstraints.BOTH;
        gbc_textPane.gridx = 0;
        gbc_textPane.gridy = 1;
        ePanel.add(neighbourNodesPane, gbc_textPane);

        lblLinkStatePakete = new JLabel("Knoten A erhält die folgenden Link State Pakete:");
        lblLinkStatePakete.setFont(new Font("Tahoma", Font.BOLD, 13));
        final GridBagConstraints gbc_lblLinkStatePakete = new GridBagConstraints();
        gbc_lblLinkStatePakete.insets = new Insets(5, 0, 5, 0);
        gbc_lblLinkStatePakete.gridx = 0;
        gbc_lblLinkStatePakete.gridy = 2;
        ePanel.add(lblLinkStatePakete, gbc_lblLinkStatePakete);

        panel = new JPanel();
        final GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.weighty = 1.0;
        gbc_panel.weightx = 1.0;
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.anchor = GridBagConstraints.NORTHWEST;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 3;
        ePanel.add(panel, gbc_panel);
        panel.setLayout(new GridLayout(0, 4, 0, 0));

        nodeBPanel = new JPanel();
        panel.add(nodeBPanel);
        nodeBPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        final GridBagLayout gbl_nodeBPanel = new GridBagLayout();
        gbl_nodeBPanel.columnWidths = new int[]
        { 0, 0, 0, 0 };
        gbl_nodeBPanel.rowHeights = new int[]
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_nodeBPanel.columnWeights = new double[]
        { 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_nodeBPanel.rowWeights = new double[]
        { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
        nodeBPanel.setLayout(gbl_nodeBPanel);

        lblB_1 = new JLabel("B");
        final GridBagConstraints gbc_lblB_1 = new GridBagConstraints();
        gbc_lblB_1.weightx = 1.0;
        gbc_lblB_1.gridwidth = 3;
        gbc_lblB_1.insets = new Insets(5, 0, 5, 0);
        gbc_lblB_1.gridx = 0;
        gbc_lblB_1.gridy = 0;
        nodeBPanel.add(lblB_1, gbc_lblB_1);

        separator_5 = new JSeparator();
        final GridBagConstraints gbc_separator_5 = new GridBagConstraints();
        gbc_separator_5.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_5.gridwidth = 3;
        gbc_separator_5.insets = new Insets(5, 0, 0, 0);
        gbc_separator_5.gridx = 0;
        gbc_separator_5.gridy = 1;
        nodeBPanel.add(separator_5, gbc_separator_5);

        lblA_1 = new JLabel("A");
        final GridBagConstraints gbc_lblA_1 = new GridBagConstraints();
        gbc_lblA_1.weightx = 1.0;
        gbc_lblA_1.insets = new Insets(5, 0, 5, 0);
        gbc_lblA_1.gridx = 0;
        gbc_lblA_1.gridy = 2;
        nodeBPanel.add(lblA_1, gbc_lblA_1);

        separator_6 = new JSeparator();
        separator_6.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_6 = new GridBagConstraints();
        gbc_separator_6.fill = GridBagConstraints.VERTICAL;
        gbc_separator_6.gridheight = 7;
        gbc_separator_6.gridx = 1;
        gbc_separator_6.gridy = 2;
        nodeBPanel.add(separator_6, gbc_separator_6);

        textPaneB_A = new JTextPane();
        textPaneB_A.setText(filler);
        textPaneDocument = textPaneB_A.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaB_A.setColumns(2);
        textPaneB_A.setFocusable(false);
        textPaneB_A.setEditable(false);
        textPaneB_A.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaB_A = new GridBagConstraints();
        gbc_textAreaB_A.fill = GridBagConstraints.HORIZONTAL;
        gbc_textAreaB_A.weightx = 1.0;
        gbc_textAreaB_A.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaB_A.gridx = 2;
        gbc_textAreaB_A.gridy = 2;
        nodeBPanel.add(textPaneB_A, gbc_textAreaB_A);

        separator_7 = new JSeparator();
        final GridBagConstraints gbc_separator_7 = new GridBagConstraints();
        gbc_separator_7.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_7.gridwidth = 3;
        gbc_separator_7.gridx = 0;
        gbc_separator_7.gridy = 3;
        nodeBPanel.add(separator_7, gbc_separator_7);

        lblB_2 = new JLabel("C");
        final GridBagConstraints gbc_lblB_2 = new GridBagConstraints();
        gbc_lblB_2.weightx = 1.0;
        gbc_lblB_2.insets = new Insets(5, 0, 5, 0);
        gbc_lblB_2.gridx = 0;
        gbc_lblB_2.gridy = 4;
        nodeBPanel.add(lblB_2, gbc_lblB_2);

        textPaneB_C = new JTextPane();
        textPaneB_C.setText(filler);
        textPaneDocument = textPaneB_C.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaB_C.setColumns(2);
        textPaneB_C.setFocusable(false);
        textPaneB_C.setEditable(false);
        textPaneB_C.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaB_C = new GridBagConstraints();
        gbc_textAreaB_C.weightx = 1.0;
        gbc_textAreaB_C.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaB_C.gridx = 2;
        gbc_textAreaB_C.gridy = 4;
        nodeBPanel.add(textPaneB_C, gbc_textAreaB_C);

        separator_8 = new JSeparator();
        final GridBagConstraints gbc_separator_8 = new GridBagConstraints();
        gbc_separator_8.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_8.gridwidth = 3;
        gbc_separator_8.gridx = 0;
        gbc_separator_8.gridy = 5;
        nodeBPanel.add(separator_8, gbc_separator_8);

        lblC_2 = new JLabel("D");
        final GridBagConstraints gbc_lblC_2 = new GridBagConstraints();
        gbc_lblC_2.weightx = 1.0;
        gbc_lblC_2.insets = new Insets(5, 0, 5, 0);
        gbc_lblC_2.gridx = 0;
        gbc_lblC_2.gridy = 6;
        nodeBPanel.add(lblC_2, gbc_lblC_2);

        textPaneB_D = new JTextPane();
        textPaneB_D.setText(filler);
        textPaneDocument = textPaneB_D.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        textPaneB_D.setFocusable(false);
        textPaneB_D.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaB_D = new GridBagConstraints();
        gbc_textAreaB_D.weightx = 1.0;
        gbc_textAreaB_D.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaB_D.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaB_D.gridx = 2;
        gbc_textAreaB_D.gridy = 6;
        nodeBPanel.add(textPaneB_D, gbc_textAreaB_D);

        separator_9 = new JSeparator();
        final GridBagConstraints gbc_separator_9 = new GridBagConstraints();
        gbc_separator_9.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_9.gridwidth = 3;
        gbc_separator_9.gridx = 0;
        gbc_separator_9.gridy = 7;
        nodeBPanel.add(separator_9, gbc_separator_9);

        lblD_2 = new JLabel("E");
        final GridBagConstraints gbc_lblD_2 = new GridBagConstraints();
        gbc_lblD_2.insets = new Insets(5, 0, 5, 0);
        gbc_lblD_2.gridx = 0;
        gbc_lblD_2.gridy = 8;
        nodeBPanel.add(lblD_2, gbc_lblD_2);

        textPaneB_E = new JTextPane();
        textPaneB_E.setText(filler);
        textPaneDocument = textPaneB_E.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneB_E.setColumns(2);
        textPaneB_E.setFocusable(false);
        textPaneB_E.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaB_E = new GridBagConstraints();
        gbc_textAreaB_E.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaB_E.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaB_E.gridx = 2;
        gbc_textAreaB_E.gridy = 8;
        nodeBPanel.add(textPaneB_E, gbc_textAreaB_E);

        nodeCPanel = new JPanel();
        panel.add(nodeCPanel);
        nodeCPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        final GridBagLayout gbl_nodeCPanel = new GridBagLayout();
        gbl_nodeCPanel.columnWidths = new int[]
        { 0, 0, 0, 0 };
        gbl_nodeCPanel.rowHeights = new int[]
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_nodeCPanel.columnWeights = new double[]
        { 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_nodeCPanel.rowWeights = new double[]
        { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
        nodeCPanel.setLayout(gbl_nodeCPanel);

        lblC_1 = new JLabel("C");
        final GridBagConstraints gbc_lblC_1 = new GridBagConstraints();
        gbc_lblC_1.weightx = 1.0;
        gbc_lblC_1.gridwidth = 3;
        gbc_lblC_1.insets = new Insets(5, 0, 5, 0);
        gbc_lblC_1.gridx = 0;
        gbc_lblC_1.gridy = 0;
        nodeCPanel.add(lblC_1, gbc_lblC_1);

        separator_10 = new JSeparator();
        final GridBagConstraints gbc_separator_10 = new GridBagConstraints();
        gbc_separator_10.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_10.gridwidth = 3;
        gbc_separator_10.insets = new Insets(5, 0, 0, 0);
        gbc_separator_10.gridx = 0;
        gbc_separator_10.gridy = 1;
        nodeCPanel.add(separator_10, gbc_separator_10);

        lblA_2 = new JLabel("A");
        final GridBagConstraints gbc_lblA_2 = new GridBagConstraints();
        gbc_lblA_2.weightx = 1.0;
        gbc_lblA_2.insets = new Insets(5, 0, 5, 0);
        gbc_lblA_2.gridx = 0;
        gbc_lblA_2.gridy = 2;
        nodeCPanel.add(lblA_2, gbc_lblA_2);

        separator_11 = new JSeparator();
        separator_11.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_11 = new GridBagConstraints();
        gbc_separator_11.fill = GridBagConstraints.VERTICAL;
        gbc_separator_11.gridheight = 7;
        gbc_separator_11.gridx = 1;
        gbc_separator_11.gridy = 2;
        nodeCPanel.add(separator_11, gbc_separator_11);

        textAreaC_A = new JTextPane();
        textAreaC_A.setText(filler);
        textPaneDocument = textAreaC_A.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaC_A.setColumns(2);
        textAreaC_A.setFocusable(false);
        textAreaC_A.setEditable(false);
        textAreaC_A.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaC_A = new GridBagConstraints();
        gbc_textAreaC_A.weightx = 1.0;
        gbc_textAreaC_A.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaC_A.gridx = 2;
        gbc_textAreaC_A.gridy = 2;
        nodeCPanel.add(textAreaC_A, gbc_textAreaC_A);

        separator_12 = new JSeparator();
        final GridBagConstraints gbc_separator_12 = new GridBagConstraints();
        gbc_separator_12.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_12.gridwidth = 3;
        gbc_separator_12.gridx = 0;
        gbc_separator_12.gridy = 3;
        nodeCPanel.add(separator_12, gbc_separator_12);

        lblB_3 = new JLabel("B");
        final GridBagConstraints gbc_lblB_3 = new GridBagConstraints();
        gbc_lblB_3.weightx = 1.0;
        gbc_lblB_3.insets = new Insets(5, 0, 5, 0);
        gbc_lblB_3.gridx = 0;
        gbc_lblB_3.gridy = 4;
        nodeCPanel.add(lblB_3, gbc_lblB_3);

        textPaneC_B = new JTextPane();
        textPaneC_B.setText(filler);
        textPaneDocument = textPaneC_B.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaC_B.setColumns(2);
        textPaneC_B.setFocusable(false);
        textPaneC_B.setEditable(false);
        textPaneC_B.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaC_B = new GridBagConstraints();
        gbc_textAreaC_B.weightx = 1.0;
        gbc_textAreaC_B.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaC_B.gridx = 2;
        gbc_textAreaC_B.gridy = 4;
        nodeCPanel.add(textPaneC_B, gbc_textAreaC_B);

        separator_13 = new JSeparator();
        final GridBagConstraints gbc_separator_13 = new GridBagConstraints();
        gbc_separator_13.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_13.gridwidth = 3;
        gbc_separator_13.gridx = 0;
        gbc_separator_13.gridy = 5;
        nodeCPanel.add(separator_13, gbc_separator_13);

        label_8 = new JLabel("D");
        final GridBagConstraints gbc_label_8 = new GridBagConstraints();
        gbc_label_8.weightx = 1.0;
        gbc_label_8.insets = new Insets(5, 0, 5, 0);
        gbc_label_8.gridx = 0;
        gbc_label_8.gridy = 6;
        nodeCPanel.add(label_8, gbc_label_8);

        textPaneC_D = new JTextPane();
        textPaneC_D.setText(filler);
        textPaneDocument = textPaneC_D.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaC_D.setColumns(2);
        textPaneC_D.setEditable(false);
        textPaneC_D.setFocusable(false);
        textPaneC_D.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaC_D = new GridBagConstraints();
        gbc_textAreaC_D.weightx = 1.0;
        gbc_textAreaC_D.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaC_D.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaC_D.gridx = 2;
        gbc_textAreaC_D.gridy = 6;
        nodeCPanel.add(textPaneC_D, gbc_textAreaC_D);

        separator_14 = new JSeparator();
        final GridBagConstraints gbc_separator_14 = new GridBagConstraints();
        gbc_separator_14.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_14.gridwidth = 3;
        gbc_separator_14.gridx = 0;
        gbc_separator_14.gridy = 7;
        nodeCPanel.add(separator_14, gbc_separator_14);

        label_9 = new JLabel("E");
        final GridBagConstraints gbc_label_9 = new GridBagConstraints();
        gbc_label_9.insets = new Insets(5, 0, 5, 0);
        gbc_label_9.gridx = 0;
        gbc_label_9.gridy = 8;
        nodeCPanel.add(label_9, gbc_label_9);

        textPaneC_E = new JTextPane();
        textPaneC_E.setText(filler);
        textPaneDocument = textPaneC_E.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textAreaC_E.setColumns(2);
        textPaneC_E.setEditable(false);
        textPaneC_E.setFocusable(false);
        textPaneC_E.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaC_E = new GridBagConstraints();
        gbc_textAreaC_E.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaC_E.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaC_E.gridx = 2;
        gbc_textAreaC_E.gridy = 8;
        nodeCPanel.add(textPaneC_E, gbc_textAreaC_E);

        nodeDPanel = new JPanel();
        panel.add(nodeDPanel);
        nodeDPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        final GridBagLayout gbl_nodeDPanel = new GridBagLayout();
        gbl_nodeDPanel.columnWidths = new int[]
        { 0, 0, 0, 0 };
        gbl_nodeDPanel.rowHeights = new int[]
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_nodeDPanel.columnWeights = new double[]
        { 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_nodeDPanel.rowWeights = new double[]
        { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
        nodeDPanel.setLayout(gbl_nodeDPanel);

        lblD_1 = new JLabel("D");
        final GridBagConstraints gbc_lblD_1 = new GridBagConstraints();
        gbc_lblD_1.weightx = 1.0;
        gbc_lblD_1.gridwidth = 3;
        gbc_lblD_1.insets = new Insets(5, 0, 5, 0);
        gbc_lblD_1.gridx = 0;
        gbc_lblD_1.gridy = 0;
        nodeDPanel.add(lblD_1, gbc_lblD_1);

        separator_15 = new JSeparator();
        final GridBagConstraints gbc_separator_15 = new GridBagConstraints();
        gbc_separator_15.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_15.gridwidth = 3;
        gbc_separator_15.insets = new Insets(5, 0, 0, 0);
        gbc_separator_15.gridx = 0;
        gbc_separator_15.gridy = 1;
        nodeDPanel.add(separator_15, gbc_separator_15);

        lblA_3 = new JLabel("A");
        final GridBagConstraints gbc_lblA_3 = new GridBagConstraints();
        gbc_lblA_3.weightx = 1.0;
        gbc_lblA_3.insets = new Insets(5, 0, 5, 0);
        gbc_lblA_3.gridx = 0;
        gbc_lblA_3.gridy = 2;
        nodeDPanel.add(lblA_3, gbc_lblA_3);

        separator_16 = new JSeparator();
        separator_16.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_16 = new GridBagConstraints();
        gbc_separator_16.fill = GridBagConstraints.VERTICAL;
        gbc_separator_16.gridheight = 7;
        gbc_separator_16.gridx = 1;
        gbc_separator_16.gridy = 2;
        nodeDPanel.add(separator_16, gbc_separator_16);

        textPaneD_A = new JTextPane();
        textPaneD_A.setText(filler);
        textPaneDocument = textPaneD_A.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneD_A.setColumns(2);
        textPaneD_A.setFocusable(false);
        textPaneD_A.setEditable(false);
        textPaneD_A.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaD_A = new GridBagConstraints();
        gbc_textAreaD_A.weightx = 1.0;
        gbc_textAreaD_A.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaD_A.gridx = 2;
        gbc_textAreaD_A.gridy = 2;
        nodeDPanel.add(textPaneD_A, gbc_textAreaD_A);

        separator_17 = new JSeparator();
        final GridBagConstraints gbc_separator_17 = new GridBagConstraints();
        gbc_separator_17.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_17.gridwidth = 3;
        gbc_separator_17.gridx = 0;
        gbc_separator_17.gridy = 3;
        nodeDPanel.add(separator_17, gbc_separator_17);

        lblB_4 = new JLabel("B");
        final GridBagConstraints gbc_lblB_4 = new GridBagConstraints();
        gbc_lblB_4.weightx = 1.0;
        gbc_lblB_4.insets = new Insets(5, 0, 5, 0);
        gbc_lblB_4.gridx = 0;
        gbc_lblB_4.gridy = 4;
        nodeDPanel.add(lblB_4, gbc_lblB_4);

        textPaneD_B = new JTextPane();
        textPaneD_B.setText(filler);
        textPaneDocument = textPaneD_B.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneD_B.setColumns(2);
        textPaneD_B.setFocusable(false);
        textPaneD_B.setEditable(false);
        textPaneD_B.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaD_B = new GridBagConstraints();
        gbc_textAreaD_B.weightx = 1.0;
        gbc_textAreaD_B.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaD_B.gridx = 2;
        gbc_textAreaD_B.gridy = 4;
        nodeDPanel.add(textPaneD_B, gbc_textAreaD_B);

        separator_18 = new JSeparator();
        final GridBagConstraints gbc_separator_18 = new GridBagConstraints();
        gbc_separator_18.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_18.gridwidth = 3;
        gbc_separator_18.gridx = 0;
        gbc_separator_18.gridy = 5;
        nodeDPanel.add(separator_18, gbc_separator_18);

        lblC_3 = new JLabel("C");
        final GridBagConstraints gbc_lblC_3 = new GridBagConstraints();
        gbc_lblC_3.weightx = 1.0;
        gbc_lblC_3.insets = new Insets(5, 0, 5, 0);
        gbc_lblC_3.gridx = 0;
        gbc_lblC_3.gridy = 6;
        nodeDPanel.add(lblC_3, gbc_lblC_3);

        textPaneD_C = new JTextPane();
        textPaneD_C.setText(filler);
        textPaneDocument = textPaneD_C.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneD_C.setColumns(2);
        textPaneD_C.setFocusable(false);
        textPaneD_C.setEditable(false);
        textPaneD_C.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaD_C = new GridBagConstraints();
        gbc_textAreaD_C.weightx = 1.0;
        gbc_textAreaD_C.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaD_C.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaD_C.gridx = 2;
        gbc_textAreaD_C.gridy = 6;
        nodeDPanel.add(textPaneD_C, gbc_textAreaD_C);

        separator_19 = new JSeparator();
        final GridBagConstraints gbc_separator_19 = new GridBagConstraints();
        gbc_separator_19.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_19.gridwidth = 3;
        gbc_separator_19.gridx = 0;
        gbc_separator_19.gridy = 7;
        nodeDPanel.add(separator_19, gbc_separator_19);

        label_14 = new JLabel("E");
        final GridBagConstraints gbc_label_14 = new GridBagConstraints();
        gbc_label_14.insets = new Insets(5, 0, 5, 0);
        gbc_label_14.gridx = 0;
        gbc_label_14.gridy = 8;
        nodeDPanel.add(label_14, gbc_label_14);

        textPaneD_E = new JTextPane();
        textPaneD_E.setText(filler);
        textPaneDocument = textPaneD_E.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneD_E.setColumns(2);
        textPaneD_E.setFocusable(false);
        textPaneD_E.setEditable(false);
        textPaneD_E.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaD_E = new GridBagConstraints();
        gbc_textAreaD_E.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaD_E.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaD_E.gridx = 2;
        gbc_textAreaD_E.gridy = 8;
        nodeDPanel.add(textPaneD_E, gbc_textAreaD_E);

        nodeEPanel = new JPanel();
        panel.add(nodeEPanel);
        nodeEPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        final GridBagLayout gbl_nodeEPanel = new GridBagLayout();
        gbl_nodeEPanel.columnWidths = new int[]
        { 0, 0, 0, 0 };
        gbl_nodeEPanel.rowHeights = new int[]
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_nodeEPanel.columnWeights = new double[]
        { 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_nodeEPanel.rowWeights = new double[]
        { 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE };
        nodeEPanel.setLayout(gbl_nodeEPanel);

        lblE_1 = new JLabel("E");
        lblE_1.setToolTipText("");
        final GridBagConstraints gbc_lblE_1 = new GridBagConstraints();
        gbc_lblE_1.weightx = 1.0;
        gbc_lblE_1.gridwidth = 3;
        gbc_lblE_1.insets = new Insets(5, 0, 5, 0);
        gbc_lblE_1.gridx = 0;
        gbc_lblE_1.gridy = 0;
        nodeEPanel.add(lblE_1, gbc_lblE_1);

        separator_20 = new JSeparator();
        final GridBagConstraints gbc_separator_20 = new GridBagConstraints();
        gbc_separator_20.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_20.gridwidth = 3;
        gbc_separator_20.insets = new Insets(5, 0, 0, 0);
        gbc_separator_20.gridx = 0;
        gbc_separator_20.gridy = 1;
        nodeEPanel.add(separator_20, gbc_separator_20);

        lblA_4 = new JLabel("A");
        final GridBagConstraints gbc_lblA_4 = new GridBagConstraints();
        gbc_lblA_4.weightx = 1.0;
        gbc_lblA_4.insets = new Insets(5, 0, 5, 0);
        gbc_lblA_4.gridx = 0;
        gbc_lblA_4.gridy = 2;
        nodeEPanel.add(lblA_4, gbc_lblA_4);

        separator_21 = new JSeparator();
        separator_21.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_21 = new GridBagConstraints();
        gbc_separator_21.fill = GridBagConstraints.VERTICAL;
        gbc_separator_21.gridheight = 7;
        gbc_separator_21.gridx = 1;
        gbc_separator_21.gridy = 2;
        nodeEPanel.add(separator_21, gbc_separator_21);

        textPaneE_A = new JTextPane();
        textPaneE_A.setText(filler);
        textPaneDocument = textPaneE_A.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneE_A.setColumns(2);
        textPaneE_A.setFocusable(false);
        textPaneE_A.setEditable(false);
        textPaneE_A.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaE_A = new GridBagConstraints();
        gbc_textAreaE_A.weightx = 1.0;
        gbc_textAreaE_A.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaE_A.gridx = 2;
        gbc_textAreaE_A.gridy = 2;
        nodeEPanel.add(textPaneE_A, gbc_textAreaE_A);

        separator_22 = new JSeparator();
        final GridBagConstraints gbc_separator_22 = new GridBagConstraints();
        gbc_separator_22.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_22.gridwidth = 3;
        gbc_separator_22.gridx = 0;
        gbc_separator_22.gridy = 3;
        nodeEPanel.add(separator_22, gbc_separator_22);

        lblB_5 = new JLabel("B");
        final GridBagConstraints gbc_lblB_5 = new GridBagConstraints();
        gbc_lblB_5.weightx = 1.0;
        gbc_lblB_5.insets = new Insets(5, 0, 5, 0);
        gbc_lblB_5.gridx = 0;
        gbc_lblB_5.gridy = 4;
        nodeEPanel.add(lblB_5, gbc_lblB_5);

        textPaneE_B = new JTextPane();
        textPaneE_B.setText(filler);
        textPaneDocument = textPaneE_B.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneE_B.setColumns(2);
        textPaneE_B.setFocusable(false);
        textPaneE_B.setEditable(false);
        textPaneE_B.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaE_B = new GridBagConstraints();
        gbc_textAreaE_B.weightx = 1.0;
        gbc_textAreaE_B.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaE_B.gridx = 2;
        gbc_textAreaE_B.gridy = 4;
        nodeEPanel.add(textPaneE_B, gbc_textAreaE_B);

        separator_23 = new JSeparator();
        final GridBagConstraints gbc_separator_23 = new GridBagConstraints();
        gbc_separator_23.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_23.gridwidth = 3;
        gbc_separator_23.gridx = 0;
        gbc_separator_23.gridy = 5;
        nodeEPanel.add(separator_23, gbc_separator_23);

        lblC_4 = new JLabel("C");
        final GridBagConstraints gbc_lblC_4 = new GridBagConstraints();
        gbc_lblC_4.weightx = 1.0;
        gbc_lblC_4.insets = new Insets(5, 0, 5, 0);
        gbc_lblC_4.gridx = 0;
        gbc_lblC_4.gridy = 6;
        nodeEPanel.add(lblC_4, gbc_lblC_4);

        textPaneE_C = new JTextPane();
        textPaneE_C.setText(filler);
        textPaneDocument = textPaneE_C.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneE_C.setColumns(2);
        textPaneE_C.setFocusable(false);
        textPaneE_C.setEditable(false);
        textPaneE_C.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaE_C = new GridBagConstraints();
        gbc_textAreaE_C.weightx = 1.0;
        gbc_textAreaE_C.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaE_C.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaE_C.gridx = 2;
        gbc_textAreaE_C.gridy = 6;
        nodeEPanel.add(textPaneE_C, gbc_textAreaE_C);

        separator_24 = new JSeparator();
        final GridBagConstraints gbc_separator_24 = new GridBagConstraints();
        gbc_separator_24.fill = GridBagConstraints.HORIZONTAL;
        gbc_separator_24.gridwidth = 3;
        gbc_separator_24.gridx = 0;
        gbc_separator_24.gridy = 7;
        nodeEPanel.add(separator_24, gbc_separator_24);

        lblD_3 = new JLabel("D");
        final GridBagConstraints gbc_lblD_3 = new GridBagConstraints();
        gbc_lblD_3.insets = new Insets(5, 0, 5, 0);
        gbc_lblD_3.gridx = 0;
        gbc_lblD_3.gridy = 8;
        nodeEPanel.add(lblD_3, gbc_lblD_3);

        textPaneE_D = new JTextPane();
        textPaneE_D.setText(filler);
        textPaneDocument = textPaneE_D.getStyledDocument();
        textPaneDocument.setParagraphAttributes(0, textPaneDocument.getLength(), center, false);
        // textPaneE_D.setColumns(2);
        textPaneE_D.setFocusable(false);
        textPaneE_D.setEditable(false);
        textPaneE_D.setBackground(backgroundColor);
        final GridBagConstraints gbc_textAreaE_D = new GridBagConstraints();
        gbc_textAreaE_D.insets = new Insets(5, 0, 5, 0);
        gbc_textAreaE_D.fill = GridBagConstraints.VERTICAL;
        gbc_textAreaE_D.gridx = 2;
        gbc_textAreaE_D.gridy = 8;
        nodeEPanel.add(textPaneE_D, gbc_textAreaE_D);

        getContentPane().add(new JSeparator(SwingConstants.HORIZONTAL));

        sPanel = new JPanel();
        getContentPane().add(sPanel);
        sPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        sInnerPanel = new JPanel();
        sPanel.add(sInnerPanel);
        final GridBagLayout gbl_sInnerPanel = new GridBagLayout();
        gbl_sInnerPanel.columnWidths = new int[]
        { 0, 100, 0, 100, 0, 100, 0 };
        gbl_sInnerPanel.rowHeights = new int[]
        { 0, 0, 20, 0, 20, 0, 0, 0, 0, 0, 0, 0 };
        gbl_sInnerPanel.columnWeights = new double[]
        { 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0 };
        gbl_sInnerPanel.rowWeights = new double[]
        { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        sInnerPanel.setLayout(gbl_sInnerPanel);

        label = new JLabel("Wegewahltabelle f\u00FCr den Knoten A");
        label.setFont(new Font("Tahoma", Font.BOLD, 13));
        final GridBagConstraints gbc_label = new GridBagConstraints();
        gbc_label.gridwidth = 7;
        gbc_label.insets = new Insets(5, 0, 10, 5);
        gbc_label.gridx = 0;
        gbc_label.gridy = 0;
        sInnerPanel.add(label, gbc_label);

        separator_29 = new JSeparator();
        final GridBagConstraints gbc_separator_29 = new GridBagConstraints();
        gbc_separator_29.fill = GridBagConstraints.BOTH;
        gbc_separator_29.gridwidth = 7;
        gbc_separator_29.insets = nullInsets;
        gbc_separator_29.gridx = 0;
        gbc_separator_29.gridy = 1;
        sInnerPanel.add(separator_29, gbc_separator_29);

        separator_3 = new JSeparator();
        separator_3.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_3 = new GridBagConstraints();
        gbc_separator_3.insets = nullInsets;
        gbc_separator_3.fill = GridBagConstraints.BOTH;
        gbc_separator_3.gridheight = 11;
        gbc_separator_3.gridx = 0;
        gbc_separator_3.gridy = 1;
        sInnerPanel.add(separator_3, gbc_separator_3);

        lblTargetNode = new JLabel("Zielknoten");
        final GridBagConstraints gbc_lblTargetNode = new GridBagConstraints();
        gbc_lblTargetNode.insets = solTitleInsets;
        gbc_lblTargetNode.gridx = 1;
        gbc_lblTargetNode.gridy = 2;
        sInnerPanel.add(lblTargetNode, gbc_lblTargetNode);

        separator_2 = new JSeparator();
        separator_2.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_2 = new GridBagConstraints();
        gbc_separator_2.fill = GridBagConstraints.BOTH;
        gbc_separator_2.gridheight = 11;
        gbc_separator_2.insets = nullInsets;
        gbc_separator_2.gridx = 2;
        gbc_separator_2.gridy = 1;
        sInnerPanel.add(separator_2, gbc_separator_2);

        separator_26 = new JSeparator();
        separator_26.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_26 = new GridBagConstraints();
        gbc_separator_26.gridheight = 11;
        gbc_separator_26.fill = GridBagConstraints.BOTH;
        gbc_separator_26.insets = nullInsets;
        gbc_separator_26.gridx = 4;
        gbc_separator_26.gridy = 1;
        sInnerPanel.add(separator_26, gbc_separator_26);

        separator_27 = new JSeparator();
        separator_27.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator_27 = new GridBagConstraints();
        gbc_separator_27.insets = nullInsets;
        gbc_separator_27.gridheight = 11;
        gbc_separator_27.fill = GridBagConstraints.BOTH;
        gbc_separator_27.gridx = 6;
        gbc_separator_27.gridy = 1;
        sInnerPanel.add(separator_27, gbc_separator_27);

        separator_28 = new JSeparator();
        final GridBagConstraints gbc_separator_28 = new GridBagConstraints();
        gbc_separator_28.gridwidth = 7;
        gbc_separator_28.fill = GridBagConstraints.BOTH;
        gbc_separator_28.insets = nullInsets;
        gbc_separator_28.gridx = 0;
        gbc_separator_28.gridy = 3;
        sInnerPanel.add(separator_28, gbc_separator_28);

        targetNodeLabelB = new JLabel("B");
        final GridBagConstraints gbc_targetNodeLabelB = new GridBagConstraints();
        gbc_targetNodeLabelB.insets = solCompInsets;
        gbc_targetNodeLabelB.gridx = 1;
        gbc_targetNodeLabelB.gridy = 4;
        sInnerPanel.add(targetNodeLabelB, gbc_targetNodeLabelB);

        separator_1 = new JSeparator();
        final GridBagConstraints gbc_separator_1 = new GridBagConstraints();
        gbc_separator_1.fill = GridBagConstraints.BOTH;
        gbc_separator_1.gridwidth = 7;
        gbc_separator_1.insets = nullInsets;
        gbc_separator_1.gridx = 0;
        gbc_separator_1.gridy = 5;
        sInnerPanel.add(separator_1, gbc_separator_1);

        targetNodeLabelC = new JLabel("C");
        final GridBagConstraints gbc_targetNodeLabelC = new GridBagConstraints();
        gbc_targetNodeLabelC.insets = solCompInsets;
        gbc_targetNodeLabelC.gridx = 1;
        gbc_targetNodeLabelC.gridy = 6;
        sInnerPanel.add(targetNodeLabelC, gbc_targetNodeLabelC);

        separator_4 = new JSeparator();
        final GridBagConstraints gbc_separator_4 = new GridBagConstraints();
        gbc_separator_4.fill = GridBagConstraints.BOTH;
        gbc_separator_4.gridwidth = 7;
        gbc_separator_4.insets = nullInsets;
        gbc_separator_4.gridx = 0;
        gbc_separator_4.gridy = 7;
        sInnerPanel.add(separator_4, gbc_separator_4);

        targetNodeLabelD = new JLabel("D");
        final GridBagConstraints gbc_targetNodeLabelD = new GridBagConstraints();
        gbc_targetNodeLabelD.insets = solCompInsets;
        gbc_targetNodeLabelD.gridx = 1;
        gbc_targetNodeLabelD.gridy = 8;
        sInnerPanel.add(targetNodeLabelD, gbc_targetNodeLabelD);

        separator_25 = new JSeparator();
        final GridBagConstraints gbc_separator_25 = new GridBagConstraints();
        gbc_separator_25.gridwidth = 7;
        gbc_separator_25.fill = GridBagConstraints.BOTH;
        gbc_separator_25.insets = nullInsets;
        gbc_separator_25.gridx = 0;
        gbc_separator_25.gridy = 9;
        sInnerPanel.add(separator_25, gbc_separator_25);

        targetNodeLabelE = new JLabel("E");
        final GridBagConstraints gbc_targetNodeLabelE = new GridBagConstraints();
        gbc_targetNodeLabelE.insets = solCompInsets;
        gbc_targetNodeLabelE.gridx = 1;
        gbc_targetNodeLabelE.gridy = 10;
        sInnerPanel.add(targetNodeLabelE, gbc_targetNodeLabelE);

        lblCost = new JLabel("Kosten");
        final GridBagConstraints gbc_lblCost = new GridBagConstraints();
        gbc_lblCost.insets = solTitleInsets;
        gbc_lblCost.gridx = 3;
        gbc_lblCost.gridy = 2;
        sInnerPanel.add(lblCost, gbc_lblCost);

        costTextField1 = new JTextField();
        costTextField1.setEditable(false);
        costTextField1.setFocusable(false);
        costTextField1.setColumns(2);
        ((AbstractDocument) costTextField1.getDocument()).setDocumentFilter(new DigitDocumentFilter(3));
        final GridBagConstraints gbc_costTextField1 = new GridBagConstraints();
        gbc_costTextField1.insets = solCompInsets;
        gbc_costTextField1.gridx = 3;
        gbc_costTextField1.gridy = 4;
        sInnerPanel.add(costTextField1, gbc_costTextField1);

        costTextField2 = new JTextField();
        costTextField2.setEditable(false);
        costTextField2.setFocusable(false);
        costTextField2.setColumns(2);
        ((AbstractDocument) costTextField2.getDocument()).setDocumentFilter(new DigitDocumentFilter(3));
        final GridBagConstraints gbc_costTextField2 = new GridBagConstraints();
        gbc_costTextField2.insets = solCompInsets;
        gbc_costTextField2.gridx = 3;
        gbc_costTextField2.gridy = 6;
        sInnerPanel.add(costTextField2, gbc_costTextField2);

        costTextField3 = new JTextField();
        costTextField3.setEditable(false);
        costTextField3.setFocusable(false);
        costTextField3.setColumns(2);
        ((AbstractDocument) costTextField3.getDocument()).setDocumentFilter(new DigitDocumentFilter(3));
        final GridBagConstraints gbc_costTextField3 = new GridBagConstraints();
        gbc_costTextField3.insets = solCompInsets;
        gbc_costTextField3.gridx = 3;
        gbc_costTextField3.gridy = 8;
        sInnerPanel.add(costTextField3, gbc_costTextField3);

        costTextField4 = new JTextField();
        costTextField4.setEditable(false);
        costTextField4.setFocusable(false);
        costTextField4.setColumns(2);
        ((AbstractDocument) costTextField4.getDocument()).setDocumentFilter(new DigitDocumentFilter(3));
        final GridBagConstraints gbc_costTextField4 = new GridBagConstraints();
        gbc_costTextField4.insets = solCompInsets;
        gbc_costTextField4.gridx = 3;
        gbc_costTextField4.gridy = 10;
        sInnerPanel.add(costTextField4, gbc_costTextField4);

        lblNextHop = new JLabel("Next Hop");
        final GridBagConstraints gbc_lblNextHop = new GridBagConstraints();
        gbc_lblNextHop.insets = solTitleInsets;
        gbc_lblNextHop.gridx = 5;
        gbc_lblNextHop.gridy = 2;
        sInnerPanel.add(lblNextHop, gbc_lblNextHop);

        nextHopTextField1 = new JTextField();
        nextHopTextField1.setEditable(false);
        nextHopTextField1.setFocusable(false);
        nextHopTextField1.setColumns(1);
        ((AbstractDocument) nextHopTextField1.getDocument()).setDocumentFilter(new AtoEUpperCaseDocumentFilter(1));
        final GridBagConstraints gbc_nextHopTextField1 = new GridBagConstraints();
        gbc_nextHopTextField1.insets = solCompInsets;
        gbc_nextHopTextField1.gridx = 5;
        gbc_nextHopTextField1.gridy = 4;
        sInnerPanel.add(nextHopTextField1, gbc_nextHopTextField1);

        nextHopTextField2 = new JTextField();
        nextHopTextField2.setEditable(false);
        nextHopTextField2.setFocusable(false);
        nextHopTextField2.setColumns(1);
        ((AbstractDocument) nextHopTextField2.getDocument()).setDocumentFilter(new AtoEUpperCaseDocumentFilter(1));
        final GridBagConstraints gbc_nextHopTextField2 = new GridBagConstraints();
        gbc_nextHopTextField2.insets = solCompInsets;
        gbc_nextHopTextField2.gridx = 5;
        gbc_nextHopTextField2.gridy = 6;
        sInnerPanel.add(nextHopTextField2, gbc_nextHopTextField2);

        nextHopTextField3 = new JTextField();
        nextHopTextField3.setEditable(false);
        nextHopTextField3.setFocusable(false);
        nextHopTextField3.setColumns(1);
        ((AbstractDocument) nextHopTextField3.getDocument()).setDocumentFilter(new AtoEUpperCaseDocumentFilter(1));
        final GridBagConstraints gbc_nextHopTextField3 = new GridBagConstraints();
        gbc_nextHopTextField3.insets = solCompInsets;
        gbc_nextHopTextField3.gridx = 5;
        gbc_nextHopTextField3.gridy = 8;
        sInnerPanel.add(nextHopTextField3, gbc_nextHopTextField3);

        nextHopTextField4 = new JTextField();
        nextHopTextField4.setEditable(false);
        nextHopTextField4.setFocusable(false);
        nextHopTextField4.setColumns(1);
        ((AbstractDocument) nextHopTextField4.getDocument()).setDocumentFilter(new AtoEUpperCaseDocumentFilter(1));
        final GridBagConstraints gbc_nextHopTextField4 = new GridBagConstraints();
        gbc_nextHopTextField4.insets = solCompInsets;
        gbc_nextHopTextField4.gridx = 5;
        gbc_nextHopTextField4.gridy = 10;
        sInnerPanel.add(nextHopTextField4, gbc_nextHopTextField4);

        separator_30 = new JSeparator();
        final GridBagConstraints gbc_separator_30 = new GridBagConstraints();
        gbc_separator_30.gridwidth = 7;
        gbc_separator_30.fill = GridBagConstraints.BOTH;
        gbc_separator_30.insets = nullInsets;
        gbc_separator_30.gridx = 0;
        gbc_separator_30.gridy = 11;
        sInnerPanel.add(separator_30, gbc_separator_30);

        separator = new JSeparator();
        getContentPane().add(separator);

        final JPanel savePanel = new JPanel();
        final GridBagLayout gbLayout_savePanel = new GridBagLayout();
        savePanel.setLayout(gbLayout_savePanel);

        locationTextField = new JTextField(System.getProperty("user.dir"));
        final GridBagConstraints gbc_locationTextField = new GridBagConstraints();
        gbc_locationTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_locationTextField.gridx = 0;
        gbc_locationTextField.gridy = 0;
        gbc_locationTextField.weightx = 1;
        gbc_locationTextField.insets = horizVertInsets;
        savePanel.add(locationTextField, gbc_locationTextField);

        browseButton = new JButton("Durchsuchen...");
        browseButton.setEnabled(false);
        final GridBagConstraints gbc_browseButton = new GridBagConstraints();
        gbc_browseButton.gridx = 1;
        gbc_browseButton.gridy = 0;
        gbc_browseButton.weightx = 0;
        browseButton.setMargin(buttonMarginInsets);
        savePanel.add(browseButton, gbc_browseButton);

        saveButton = new JButton("Speichern");
        saveButton.setEnabled(false);
        final GridBagConstraints gbc_saveButton = new GridBagConstraints();
        gbc_saveButton.gridx = 2;
        gbc_saveButton.gridy = 0;
        gbc_saveButton.weightx = 0;
        saveButton.setMargin(buttonMarginInsets);
        savePanel.add(saveButton, gbc_saveButton);

        getContentPane().add(savePanel);

        setResizable(false);
        setSize(450, 700);
        setLocation(400, 100);
        setVisible(true);

        // add listeners
        matNrTextField.addKeyListener(new InputValidator(matNrTextField, loadButton));

        matNrTextField.addCaretListener(new CaretListener()
        {
            @Override
            public void caretUpdate(final CaretEvent e)
            {
                if (matNrTextField.getText().equals(Constants.PROMPT))
                {
                    matNrTextField.setCaretPosition(0);
                }

            }
        });

        matNrTextField.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusGained(final FocusEvent e)
            {
                getRootPane().setDefaultButton(loadButton);
                super.focusGained(e);
            }
        });

        loadButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                String input = matNrTextField.getText();
                if (!input.equals(Constants.PROMPT))
                {
                    input = input.replaceAll("\\s", ""); // remove whitespaces

                    final Pattern pattern = Pattern.compile("^([0-9]){6}$");
                    final Matcher matcher = pattern.matcher(input);
                    if (matcher.find())
                    {
                        indexOfMatNr = matrikelNummern.get(input);
                        if ((indexOfMatNr != null))
                        {
                            exerciseLoad(data[indexOfMatNr]);
                            return;
                        }
                    }
                    exerciseUnload();
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Matrikelnummer " + input + " ist ungültig", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                exerciseUnload();
            }

        });

        browseButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                final File path = new File(locationTextField.getText());
                final JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(path);
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if (fileChooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
                {
                    locationTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        saveButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(final ActionEvent e)
            {
                final String location = locationTextField.getText();
                final String fileName = location.charAt(location.length() - 1) == File.separatorChar ? location + data[indexOfMatNr][Constants.O_COL_M_NR] + Constants.STUD_CRC_SOL_FNAME_ADD : location + File.separator + data[indexOfMatNr][Constants.O_COL_M_NR] + Constants.STUD_LSR_SOL_FNAME_ADD;
                try (FileOutputStream fos = new FileOutputStream(fileName))
                {
                    fos.write(costTextField1.getText().concat(" ").concat(nextHopTextField1.getText()).concat(lineSeparator).getBytes());
                    fos.write(costTextField2.getText().concat(" ").concat(nextHopTextField2.getText()).concat(lineSeparator).getBytes());
                    fos.write(costTextField3.getText().concat(" ").concat(nextHopTextField3.getText()).concat(lineSeparator).getBytes());
                    fos.write(costTextField4.getText().concat(" ").concat(nextHopTextField4.getText()).getBytes());
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Die Lösung wurde erfolgreich in der Datei \'" + fileName + "\' abgespeichert.", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
                }
                catch (final IOException ioe)
                {
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Konnte nicht in die datei \'" + fileName + "\' schreiben.", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private String formatNeighbourString()
    {
        return String.format(neighbourPaneString, a_b, a_c);
    }

    private void exerciseLoad(final String data[])
    {
        final String[] nodeAvalues = data[Constants.O_COL_A_NODE].replaceAll("\\b0\\b", "-").split(" ");
        final String[] nodeBvalues = data[Constants.O_COL_B_NODE].replaceAll("\\b0\\b", "-").split(" ");
        final String[] nodeCvalues = data[Constants.O_COL_C_NODE].replaceAll("\\b0\\b", "-").split(" ");
        final String[] nodeDvalues = data[Constants.O_COL_D_NODE].replaceAll("\\b0\\b", "-").split(" ");
        final String[] nodeEvalues = data[Constants.O_COL_E_NODE].replaceAll("\\b0\\b", "-").split(" ");
        a_b = nodeAvalues[0];
        a_c = nodeAvalues[1];
        neighbourNodesPane.setText(formatNeighbourString());
        textPaneB_A.setText(nodeBvalues[0]);
        textPaneB_C.setText(nodeBvalues[1]);
        textPaneB_D.setText(nodeBvalues[2]);
        textPaneB_E.setText(nodeBvalues[3]);
        textAreaC_A.setText(nodeCvalues[0]);
        textPaneC_B.setText(nodeCvalues[1]);
        textPaneC_D.setText(nodeCvalues[2]);
        textPaneC_E.setText(nodeCvalues[3]);
        textPaneD_A.setText(nodeDvalues[0]);
        textPaneD_B.setText(nodeDvalues[1]);
        textPaneD_C.setText(nodeDvalues[2]);
        textPaneD_E.setText(nodeDvalues[3]);
        textPaneE_A.setText(nodeEvalues[0]);
        textPaneE_B.setText(nodeEvalues[1]);
        textPaneE_C.setText(nodeEvalues[2]);
        textPaneE_D.setText(nodeEvalues[3]);

        costTextField1.setEditable(true);
        costTextField2.setEditable(true);
        costTextField3.setEditable(true);
        costTextField4.setEditable(true);
        nextHopTextField1.setEditable(true);
        nextHopTextField2.setEditable(true);
        nextHopTextField3.setEditable(true);
        nextHopTextField4.setEditable(true);

        costTextField1.setFocusable(true);
        costTextField2.setFocusable(true);
        costTextField3.setFocusable(true);
        costTextField4.setFocusable(true);
        nextHopTextField1.setFocusable(true);
        nextHopTextField2.setFocusable(true);
        nextHopTextField3.setFocusable(true);
        nextHopTextField4.setFocusable(true);

        browseButton.setEnabled(true);
        saveButton.setEnabled(true);

        getRootPane().setDefaultButton(saveButton);
        costTextField1.requestFocus();
    }

    private void exerciseUnload()
    {
        browseButton.setEnabled(false);
        saveButton.setEnabled(false);
        getRootPane().setDefaultButton(loadButton);
        textPaneB_A.setText(filler);
        textPaneB_C.setText(filler);
        textPaneB_D.setText(filler);
        textPaneB_E.setText(filler);
        textAreaC_A.setText(filler);
        textPaneC_B.setText(filler);
        textPaneC_D.setText(filler);
        textPaneC_E.setText(filler);
        textPaneD_A.setText(filler);
        textPaneD_B.setText(filler);
        textPaneD_C.setText(filler);
        textPaneD_E.setText(filler);
        textPaneE_A.setText(filler);
        textPaneE_B.setText(filler);
        textPaneE_C.setText(filler);
        textPaneE_D.setText(filler);

        costTextField1.setEditable(false);
        costTextField2.setEditable(false);
        costTextField3.setEditable(false);
        costTextField4.setEditable(false);
        nextHopTextField1.setEditable(false);
        nextHopTextField2.setEditable(false);
        nextHopTextField3.setEditable(false);
        nextHopTextField4.setEditable(false);

        costTextField1.setFocusable(false);
        costTextField2.setFocusable(false);
        costTextField3.setFocusable(false);
        costTextField4.setFocusable(false);
        nextHopTextField1.setFocusable(false);
        nextHopTextField2.setFocusable(false);
        nextHopTextField3.setFocusable(false);
        nextHopTextField4.setFocusable(false);
    }
}