/*
 * CRCExercise
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.gui.crc;

import static de.hs_trier.rn.utils.MiscUtils.convertBoolToInt;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.omg.CORBA.portable.ApplicationException;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.csv.CSVReader;
import de.hs_trier.rn.utils.gui.InputValidator;

public class CRCExercise extends JFrame
{
    /**
	 * 
	 */
    private static final long serialVersionUID = -306888616716275814L;

    private static final String INPUT_FILE = "crc_data.csv";

    private String[][] data;
    
    private Map<String, Integer> matrikelNummern;

    private Integer indexOfMatNr;

    private final JTextArea generatorTextArea;

    private JTextArea message1TextArea;

    private JTextArea message2TextArea;

    private JButton loadButton;

    private JButton browseButton;

    private JButton saveButton;

    private JTextField solution1TextField;

    private JTextField solution2TextField;

    private JCheckBox questionCheckBox;

    private JTextField locationTextField;

    private final String lineSeparator = System.getProperty("line.separator");

    public CRCExercise(final String title)
    {
        super(title);

        // read data from file
        try
        {
            data = CSVReader.readCSV(getClass().getClassLoader().getResourceAsStream(INPUT_FILE), Constants.CRC_O_COL_NUM);
        }
        catch (final Exception e)
        {
            JOptionPane.showMessageDialog(null, "Die Aufgabendaten konnten nicht geladen werden.", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        
        matrikelNummern = new HashMap<>();
        for (int i = 0; i < data.length; i++)
        {
            matrikelNummern.put(data[i][Constants.O_COL_M_NR], i);
        }

        // Build GUI
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // Prepare Layouts
        final BoxLayout bLayout = new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS);

        final Insets horizInsets = new Insets(0, 5, 0, 5);
        final Insets horizVertInsets = new Insets(6, 5, 6, 5);
        final Insets generatorTextAreaInsets = new Insets(6, 5, 6, 32);
        final Insets message1TextAreaInsets = new Insets(6, 5, 6, 32);
        final Insets message2TextAreaInsets = new Insets(6, 5, 6, 32);
        // final Insets nullInsets = new Insets(0, 0, 0, 0);
        final Insets buttonMarginInsets = new Insets(-1, 0, -2, 0);

        setLayout(bLayout);

        // Exercise loading panel
        final JPanel eLoadPanel = new JPanel();
        final GridBagLayout gbLayout_eLoadPanel = new GridBagLayout();
        eLoadPanel.setLayout(gbLayout_eLoadPanel);

        final JTextField matNrTextField = new JTextField(Constants.PROMPT);
        matNrTextField.setForeground(Color.GRAY);
        matNrTextField.setHighlighter(null);
        final GridBagConstraints gbc_matNrTextField = new GridBagConstraints();
        gbc_matNrTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_matNrTextField.gridx = 0;
        gbc_matNrTextField.gridy = 0;
        gbc_matNrTextField.weightx = 1;
        gbc_matNrTextField.insets = new Insets(0, 210, 0, 0);
        eLoadPanel.add(matNrTextField, gbc_matNrTextField);

        loadButton = new JButton("Aufgabe laden");
        loadButton.setEnabled(false);
        final GridBagConstraints gbc_loadButton = new GridBagConstraints();
        gbc_loadButton.fill = GridBagConstraints.NONE;
        gbc_loadButton.gridx = 1;
        gbc_loadButton.gridy = 0;
        gbc_loadButton.weightx = 0;
        gbc_loadButton.weighty = 0;
        gbc_loadButton.insets = horizVertInsets;
        loadButton.setMargin(buttonMarginInsets);
        eLoadPanel.add(loadButton, gbc_loadButton);

        add(eLoadPanel);

        add(new JSeparator(SwingConstants.HORIZONTAL));

        final JPanel midPanel = new JPanel();
        final GridBagLayout gbLayout_midPanel = new GridBagLayout();
        midPanel.setLayout(gbLayout_midPanel);

        // Generator Polynom
        final JLabel generatorLabel = new JLabel("Generatorpolynom: ", SwingConstants.RIGHT);
        final GridBagConstraints gbc_generatorLabel = new GridBagConstraints();
        gbc_generatorLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_generatorLabel.gridx = 0;
        gbc_generatorLabel.gridy = 0;
        gbc_generatorLabel.insets = horizVertInsets;
        midPanel.add(generatorLabel, gbc_generatorLabel);

        generatorTextArea = new JTextArea();
        generatorTextArea.setEditable(false);
        generatorTextArea.setFocusable(false);
        final GridBagConstraints gbc_generatorTextArea = new GridBagConstraints();
        gbc_generatorTextArea.fill = GridBagConstraints.HORIZONTAL;
        gbc_generatorTextArea.gridx = 1;
        gbc_generatorTextArea.gridy = 0;
        gbc_generatorTextArea.weightx = 1; // important
        gbc_generatorTextArea.insets = generatorTextAreaInsets;
        midPanel.add(generatorTextArea, gbc_generatorTextArea);

        JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
        GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.gridx = 0;
        gbc_separator.gridy = 2;
        gbc_separator.gridwidth = 2;
        gbc_separator.fill = GridBagConstraints.HORIZONTAL;
        midPanel.add(separator, gbc_separator);

        // // Task1 - Calculate transferred message
        final JLabel message1Label = new JLabel("Die zu übertragende Nachricht: ", SwingConstants.RIGHT);
        final GridBagConstraints gbc_message1Label = new GridBagConstraints();
        gbc_message1Label.fill = GridBagConstraints.HORIZONTAL;
        gbc_message1Label.gridx = 0;
        gbc_message1Label.gridy = 3;
        gbc_message1Label.insets = horizVertInsets;
        midPanel.add(message1Label, gbc_message1Label);

        message1TextArea = new JTextArea();
        message1TextArea.setEditable(false);
        message1TextArea.setFocusable(false);
        final GridBagConstraints gbc_message1TextArea = new GridBagConstraints();
        gbc_message1TextArea.fill = GridBagConstraints.HORIZONTAL;
        gbc_message1TextArea.gridx = 1;
        gbc_message1TextArea.gridy = 3;
        gbc_message1TextArea.insets = message1TextAreaInsets;
        midPanel.add(message1TextArea, gbc_message1TextArea);

        final JLabel solution1Label = new JLabel("Übertragene Nachricht inkl. redundante Bits: ", SwingConstants.RIGHT);
        final GridBagConstraints gbc_solution1Label = new GridBagConstraints();
        gbc_solution1Label.fill = GridBagConstraints.HORIZONTAL;
        gbc_solution1Label.gridx = 0;
        gbc_solution1Label.gridy = 4;
        gbc_solution1Label.insets = horizVertInsets;
        midPanel.add(solution1Label, gbc_solution1Label);

        solution1TextField = new JTextField();
        final GridBagConstraints gbc_solution1TextField = new GridBagConstraints();
        gbc_solution1TextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_solution1TextField.gridx = 1;
        gbc_solution1TextField.gridy = 4;
        gbc_solution1TextField.insets = message1TextAreaInsets;
        midPanel.add(solution1TextField, gbc_solution1TextField);

        separator = new JSeparator(SwingConstants.HORIZONTAL);
        gbc_separator = new GridBagConstraints();
        gbc_separator.gridx = 0;
        gbc_separator.gridy = 5;
        gbc_separator.gridwidth = 2;
        gbc_separator.fill = GridBagConstraints.HORIZONTAL;
        midPanel.add(separator, gbc_separator);

        // Task 2 - Calculate the rest of transferred polynom
        final JLabel message2Label = new JLabel("Die empfangene Nachricht: ", SwingConstants.RIGHT);
        final GridBagConstraints gbc_message2Label = new GridBagConstraints();
        gbc_message2Label.fill = GridBagConstraints.HORIZONTAL;
        gbc_message2Label.gridx = 0;
        gbc_message2Label.gridy = 6;
        gbc_message2Label.insets = horizVertInsets;
        midPanel.add(message2Label, gbc_message2Label);

        message2TextArea = new JTextArea();
        message2TextArea.setEditable(false);
        message2TextArea.setFocusable(false);
        final GridBagConstraints gbc_message2TextArea = new GridBagConstraints();
        gbc_message2TextArea.fill = GridBagConstraints.HORIZONTAL;
        gbc_message2TextArea.gridx = 1;
        gbc_message2TextArea.gridy = 6;
        gbc_message2TextArea.insets = message2TextAreaInsets;
        midPanel.add(message2TextArea, gbc_message2TextArea);

        final JLabel solution2Label = new JLabel("Divisionsrest: ", SwingConstants.RIGHT);
        final GridBagConstraints gbc_solution2Label = new GridBagConstraints();
        gbc_solution2Label.fill = GridBagConstraints.HORIZONTAL;
        gbc_solution2Label.gridx = 0;
        gbc_solution2Label.gridy = 7;
        gbc_solution2Label.insets = horizVertInsets;
        midPanel.add(solution2Label, gbc_solution2Label);

        solution2TextField = new JTextField();
        final GridBagConstraints gbc_solution2TextField = new GridBagConstraints();
        gbc_solution2TextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_solution2TextField.gridx = 1;
        gbc_solution2TextField.gridy = 7;
        gbc_solution2TextField.insets = message2TextAreaInsets;
        midPanel.add(solution2TextField, gbc_solution2TextField);

        final JLabel questionLabel = new JLabel(Constants.QUESTION);
        final GridBagConstraints gbc_questionLabel = new GridBagConstraints();
        gbc_questionLabel.fill = GridBagConstraints.HORIZONTAL;
        gbc_questionLabel.gridx = 0;
        gbc_questionLabel.gridy = 8;
        gbc_questionLabel.insets = horizInsets;
        midPanel.add(questionLabel, gbc_questionLabel);

        questionCheckBox = new JCheckBox();
        questionCheckBox.setEnabled(false);
        final GridBagConstraints gbc_questionCheckBox = new GridBagConstraints();
        gbc_questionCheckBox.gridx = 1;
        gbc_questionCheckBox.gridy = 8;
        gbc_questionCheckBox.anchor = GridBagConstraints.WEST;
        midPanel.add(questionCheckBox, gbc_questionCheckBox);

        add(midPanel);

        add(new JSeparator(SwingConstants.HORIZONTAL));

        // Save Panel
        final JPanel savePanel = new JPanel();
        final GridBagLayout gbLayout_savePanel = new GridBagLayout();
        savePanel.setLayout(gbLayout_savePanel);

        locationTextField = new JTextField(System.getProperty("user.dir"));
        final GridBagConstraints gbc_locationTextField = new GridBagConstraints();
        gbc_locationTextField.fill = GridBagConstraints.HORIZONTAL;
        gbc_locationTextField.gridx = 0;
        gbc_locationTextField.gridy = 0;
        gbc_locationTextField.weightx = 1;
        gbc_locationTextField.insets = horizVertInsets;
        savePanel.add(locationTextField, gbc_locationTextField);

        browseButton = new JButton("Durchsuchen...");
        browseButton.setEnabled(false);
        final GridBagConstraints gbc_browseButton = new GridBagConstraints();
        gbc_browseButton.gridx = 1;
        gbc_browseButton.gridy = 0;
        gbc_browseButton.weightx = 0;
        browseButton.setMargin(buttonMarginInsets);
        savePanel.add(browseButton, gbc_browseButton);

        saveButton = new JButton("Speichern");
        saveButton.setEnabled(false);
        final GridBagConstraints gbc_saveButton = new GridBagConstraints();
        gbc_saveButton.gridx = 2;
        gbc_saveButton.gridy = 0;
        gbc_saveButton.weightx = 0;
        gbc_saveButton.insets = new Insets(6, 5, 6, 5);
        saveButton.setMargin(buttonMarginInsets);
        savePanel.add(saveButton, gbc_saveButton);

        add(savePanel);

        setResizable(false);
        setSize(480, 290);
        setVisible(true);

        // add listeners
        matNrTextField.addKeyListener(new InputValidator(matNrTextField, loadButton));

        matNrTextField.addCaretListener(new CaretListener()
        {
            @Override
            public void caretUpdate(final CaretEvent e)
            {
                if (matNrTextField.getText().equals(Constants.PROMPT))
                {
                    matNrTextField.setCaretPosition(0);
                }

            }
        });

        matNrTextField.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusGained(final FocusEvent e)
            {
                getRootPane().setDefaultButton(loadButton);
                super.focusGained(e);
            }
        });

        solution1TextField.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusGained(final FocusEvent e)
            {
                if (browseButton.isEnabled())
                {
                    getRootPane().setDefaultButton(saveButton);
                }
                super.focusGained(e);
            }
        });

        loadButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                String input = matNrTextField.getText();
                if (!input.equals(Constants.PROMPT))
                {
                    input = input.replaceAll("\\s", ""); // remove whitespaces

                    final Pattern pattern = Pattern.compile("^([0-9]){6}$");
                    final Matcher matcher = pattern.matcher(input);
                    if (matcher.find())
                    {
                        indexOfMatNr = matrikelNummern.get(input);
                        if ((indexOfMatNr != null))
                        {
                            exerciseLoad(data[indexOfMatNr][Constants.O_COL_G_POL], data[indexOfMatNr][Constants.O_COL_M_POL_1], data[indexOfMatNr][Constants.O_COL_T_POL_1]);
                            return;
                        }
                    }
                    exerciseUnload();
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Matrikelnummer " + input + " ist ungültig", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                exerciseUnload();
            }

        });

        browseButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(final ActionEvent e)
            {
                final File path = new File(locationTextField.getText());
                final JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(path);
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                if (fileChooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION)
                {
                    locationTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        saveButton.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(final ActionEvent e)
            {
                final String location = locationTextField.getText();
                final String fileName = location.charAt(location.length() - 1) == File.separatorChar ? location + data[indexOfMatNr][Constants.O_COL_M_NR] + Constants.STUD_CRC_SOL_FNAME_ADD : location + File.separator + data[indexOfMatNr][Constants.O_COL_M_NR] + Constants.STUD_CRC_SOL_FNAME_ADD;
                try (FileOutputStream fos = new FileOutputStream(fileName))
                {
                    fos.write(solution1TextField.getText().concat(lineSeparator).getBytes());
                    fos.write(solution2TextField.getText().concat(lineSeparator).getBytes());
                    fos.write(String.valueOf(convertBoolToInt(questionCheckBox.isSelected())).concat(lineSeparator).getBytes());
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Die Lösung wurde erfolgreich in der Datei \'" + fileName + "\' abgespeichert.", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
                }
                catch (final IOException ioe)
                {
                    JOptionPane.showMessageDialog((Component) e.getSource(), "Konnte nicht in die datei \'" + fileName + "\' schreiben.", "Fehlermeldung", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void exerciseLoad(final String generator, final String message1, final String message2)

    {
        generatorTextArea.setText(generator);
        message1TextArea.setText(message1);
        message2TextArea.setText(message2);
        browseButton.setEnabled(true);
        saveButton.setEnabled(true);
        questionCheckBox.setEnabled(true);
        getRootPane().setDefaultButton(saveButton);
        solution1TextField.requestFocus();
    }

    private void exerciseUnload()
    {
        browseButton.setEnabled(false);
        saveButton.setEnabled(false);
        getRootPane().setDefaultButton(loadButton);
        message1TextArea.setText("");
        generatorTextArea.setText("");
        questionCheckBox.setEnabled(false);
    }
}