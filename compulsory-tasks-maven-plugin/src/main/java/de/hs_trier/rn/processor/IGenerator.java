package de.hs_trier.rn.processor;

public interface IGenerator {
    String generate(Object argument);
}