package de.hs_trier.rn.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.jnetpcap.Pcap;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.JPacketHandler;
import org.jnetpcap.protocol.tcpip.Tcp;
import org.jnetpcap.protocol.tcpip.Udp;

import de.hs_trier.rn.plugin.nativeloader.CustomClassLoader;
import de.hs_trier.rn.plugin.nativeloader.NativeLoader;
import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;
import de.hs_trier.rn.utils.csv.CSVReader;

@Mojo(name = "evaluate-pcap-uploads", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class PCAPUploadEvaluationMojo extends AbstractUploadEvaluationMojo
{
    @Parameter(defaultValue = "${basedir}/../resources/${semester}", property = "inputFolder")
    private String inputFolder;
    
    @Parameter(defaultValue = "${basedir}/../resources/${semester}/uploads", property = "uploadsFolder")
    private String uploadsFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/pcap-evaluation", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "TeilnehmerInnen_Rechnernetze", property = "inputFileName")
    private String inputFileName;

    @Parameter(defaultValue = "pcap_evaluation_data", property = "outputFileName")
    private String outputFileName;

    private NativeLoader nativeLoader;
    
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        CustomClassLoader cl;
        Class<?> ca;
        Object a;
        Method p;
        try
        {
//            cl = new CustomClassLoader();
//            ca = cl.findClass("de.hs_trier.rn.plugin.nativeloader.NativeLoader");
//            ctrs = ca.getConstructors();
//            a = ctrs[0].newInstance();
//            p = ca.getMethod("loadLibrary", new Class[]{String.class});
//            p.invoke(a, "jnetpcap");

            new NativeLoader().loadLibrary("jnetpcap");
            super.execute();

//            p = ca.getMethod("getLibFiles");
//            Set<File> libFiles = ((Set<File>) p.invoke(a));
//            p = null;
//            a = null;
//            ca = null;
//            cl = null;
//            System.gc();
//            for (File file : libFiles)
//            {
//                file.delete();
//            }
        }
        catch(Exception e)
        {
            throw new MojoExecutionException("", e);
        }
    }

    @Override
    protected String getIdentifier()
    {
        return "PCAP";
    }

    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }

    @Override
    protected String getUploadsFolder()
    {
        return uploadsFolder;
    }

    @Override
    protected int getNumberOfColumnsInInputFile()
    {
        return 1;
    }

    @Override protected int[] getSolutionColumns() { return new int[]{-1}; } // NOT
    @Override protected CSVHandler getCSVHandler() { return null; } //              USED

    @Override
    protected String[] getOutputHeader()
    {
        return new String[]{"Mnr", "Gefunden?"};
    }

    @Override
    protected String getInputFileName()
    {
        return inputFileName;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getOutputFileName()
    {
        return outputFileName;
    }
    
    @Override
    protected String[][] readData(int numberOfAdditionalColumns) throws Exception
    {
        return CSVReader.readCSV(MiscUtils.assembleFileName(getInputFolder(), getInputFileName(), Constants.CSV_EXT), new int[]{Constants.F_COL_M_NR}, true, getNumberOfColumnsInInputFile() + numberOfAdditionalColumns);
    }

    @Override
    protected void processFile(File f, int index) throws IOException
    {        
        final AtomicBoolean matNrFound = new AtomicBoolean();
        final Pattern REGEX = Pattern.compile(".*" + data[index][0] + ".*", Pattern.DOTALL);
        final StringBuilder errbuf = new StringBuilder();
        final Pcap pcap = Pcap.openOffline(f.getAbsolutePath(), errbuf);
        if (pcap != null)
        {
            pcap.loop(Pcap.LOOP_INFINITE, new JPacketHandler<StringBuilder>()
            {

                final Tcp tcp = new Tcp();

                final Udp udp = new Udp();

                @Override
                public void nextPacket(final JPacket packet, final StringBuilder errbuf)
                {
                    if (packet.hasHeader(tcp))
                    {
                        final String payload = new String(tcp.getPayload());
                        matNrFound.set(REGEX.matcher(payload).matches());
                    }
                    else if (packet.hasHeader(udp))
                    {
                        final String payload = new String(udp.getPayload());
                        matNrFound.set(REGEX.matcher(payload).matches());
                    }
                }
            }, errbuf);
            if (errbuf.length() > 0)
                getLog().warn(errbuf.toString());
            data[index][1] = matNrFound.get() ? "=true" : "=false";
        }
        else
        {
            getLog().warn("Could not read " + f.getAbsolutePath());
            getLog().warn("The following error occured: " + errbuf.toString());
        }
    }
}
