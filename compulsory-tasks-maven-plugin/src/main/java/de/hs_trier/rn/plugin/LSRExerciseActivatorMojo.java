/*
 * LSRExerciseActivatorMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.processor.IGenerator;
import de.hs_trier.rn.processor.LSRExerciseActivatorGenerator;

@Mojo(name = "generate-lsr-exercise-activator", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class LSRExerciseActivatorMojo extends AbstractExerciseActivatorMojo
{
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/activator", property = "sourceFolder")
    private String sourceFolder;

    @Parameter(defaultValue = "${semester}", required = true, property = "semester")
    private String semester;

    @Parameter(defaultValue = "de.hs_trier.rn.gui.lsr", property = "packageName")
    private String packageName;

    @Parameter(defaultValue = "LSRExerciseActivator", property = "className")
    private String className;
    
    @Override
    protected IGenerator getGenerator()
    {
        return new LSRExerciseActivatorGenerator();
    }

    @Override
    protected String getIdentifier()
    {
        return "LSR";
    }

    @Override
    protected String getSourceFolder()
    {
        return sourceFolder;
    }

    @Override
    protected String getSemester()
    {
        return semester;
    }

    @Override
    protected String getPackageName()
    {
        return packageName;
    }

    @Override
    protected String getClassName()
    {
        return className;
    }
    
}
