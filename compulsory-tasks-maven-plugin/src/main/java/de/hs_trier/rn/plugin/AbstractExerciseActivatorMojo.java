/*
 * AbstractExerciseActivatorMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import java.io.File;
import java.io.PrintWriter;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import de.hs_trier.rn.processor.IGenerator;
import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.TableWriter;

public abstract class AbstractExerciseActivatorMojo extends AbstractMojo
{
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        IGenerator generator = getGenerator();
        String exerciseActivatorContent = generator.generate(getSemester().toUpperCase());
        String exerciseActivatorFilePath = MiscUtils.assembleFileName(getSourceFolder() + File.separator + getPackageName().replace('.', File.separator.charAt(0)), getClassName(), Constants.JAVA_EXT);
        try
        {
            TableWriter.prepareFolder(exerciseActivatorFilePath);
            try (PrintWriter pw = new PrintWriter(exerciseActivatorFilePath))
            {
                pw.print(exerciseActivatorContent);
                getLog().info("Generated " + getIdentifier() + " exercise activator " + getPackageName() + "." + getClassName());
            }
        }
        catch (Exception e)
        {
            throw new MojoExecutionException(Constants.EMPTYSTR, e);
        }
    }

    protected abstract IGenerator getGenerator();

    protected abstract String getIdentifier();

    protected abstract String getSourceFolder();

    protected abstract String getSemester();

    protected abstract String getPackageName();

    protected abstract String getClassName();
}
