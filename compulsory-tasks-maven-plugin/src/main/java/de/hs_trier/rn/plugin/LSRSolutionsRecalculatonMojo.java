/*
 * LSRSolutionsRecalculatonMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.MiscUtils.createRangeArray;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "recalculate-lsr-solutions", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresProject = false)
public class LSRSolutionsRecalculatonMojo extends AbstractSolutionsRecalculatonMojo
{
    @Parameter(defaultValue = "${basedir}/../resources/${semester}/lsr-tasks", property = "inputFolder")
    private String inputFolder;
    
    @Parameter(defaultValue = "${basedir}/../compulsory-tasks-gui-lsr/target/pflichtabgabe_lsr_rn_${semester}.jar", property = "jarPath")
    private String jarPath;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/lsr-tasks", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "lsr_data", property = "tasksFileName")
    private String tasksFileName;

    @Parameter(defaultValue = "lsr_solution_data_recovery", property = "tasksAndSolutionsFileName")
    private String tasksAndSolutionsFileName;
    
    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }

    @Override
    protected String getJARPath()
    {
        return jarPath;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getTasksFileName()
    {
        return tasksFileName;
    }

    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getLSRInstance();
    }

    @Override
    protected String getTasksAndSolutionsFileName()
    {
        return tasksAndSolutionsFileName;
    }

    @Override
    protected int[] getElementIndices()
    {
        return createRangeArray(getNumberOfColumnsToRead());
    }

    @Override
    protected int getNumberOfColumnsToRead()
    {
        return Constants.LSR_O_COL_NUM;
    }
}
