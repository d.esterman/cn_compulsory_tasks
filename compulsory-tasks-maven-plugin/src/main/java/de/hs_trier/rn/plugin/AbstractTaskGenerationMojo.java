/*
 * CRCTaskGenerationMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.Constants.F_COL_NUM;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;
import de.hs_trier.rn.utils.csv.CSVReader;

/**
 * Liest eine Teilnehmerliste im CSV-Format mit Matrikelnummern und erzeugt zwei
 * Dateien: Eine für den Prüfer mit allen Informationen und eine für das
 * veröffentlichte Tool (ohne Musterlösungen).
 * 
 */
public abstract class AbstractTaskGenerationMojo extends AbstractMojo
{
    @Parameter(defaultValue = "TeilnehmerInnen_Rechnernetze", property = "participantsFileName")
    protected String participantsFileName;
    
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            String[][] inputData = CSVReader.readCSV(MiscUtils.assembleFileName(getInputFolder(), participantsFileName, Constants.CSV_EXT), F_COL_NUM);
            CSVHandler csvHandler = getCSVHandler();
            final String[][] outputData = csvHandler.generateExercises(inputData);
            csvHandler.writeXLSXWithSolution(MiscUtils.assembleFileName(getOutputFolder(), getTasksAndSolutionsFileName(), Constants.XLSX_EXT), outputData);
            getLog().info("Created " + getIdentifier() + " tasks at " + getOutputFolder() + "/" + getTasksAndSolutionsFileName());
            csvHandler.writeCSVWithoutSolution(MiscUtils.assembleFileName(getOutputFolder(), getTasksFileName(), Constants.CSV_EXT), getElementIndicies(), outputData);
            getLog().info("Created " + getIdentifier() + " tasks with solutions at " + getOutputFolder() + "/" + getTasksAndSolutionsFileName());
        }
        catch (Exception e)
        {
            throw new MojoExecutionException(Constants.EMPTYSTR, e);
        }
    }

    protected abstract CSVHandler getCSVHandler();
    protected abstract String getIdentifier();
    protected abstract String getInputFolder();
    protected abstract String getOutputFolder();
    protected abstract String getTasksFileName();
    protected abstract int[] getElementIndicies();
    protected abstract String getTasksAndSolutionsFileName();
}
