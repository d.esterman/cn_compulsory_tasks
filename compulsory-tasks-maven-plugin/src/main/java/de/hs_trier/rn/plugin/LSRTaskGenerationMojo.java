/*
 * LSRTaskGenerationMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.MiscUtils.createRangeArray;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "generate-lsr-tasks", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class LSRTaskGenerationMojo extends AbstractTaskGenerationMojo
{
    @Parameter(defaultValue = "${semester}", required = true, property = "semester")
    private String semester;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}", property = "inputFolder")
    private String inputFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/lsr-tasks", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "lsr_data", property = "tasksFileName")
    private String tasksFileName;

    @Parameter(defaultValue = "lsr_solution_data", property = "tasksAndSolutionsFileName")
    private String tasksAndSolutionsFileName;

    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getLSRInstance();
    }

    @Override
    protected String getIdentifier()
    {
        return "LSR";
    }

    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getTasksFileName()
    {
        return tasksFileName;
    }

    @Override
    protected int[] getElementIndicies()
    {
        return createRangeArray(getCSVHandler().getHeaderWithoutSolution().length);
    }

    @Override
    protected String getTasksAndSolutionsFileName()
    {
        return tasksAndSolutionsFileName;
    }
}
