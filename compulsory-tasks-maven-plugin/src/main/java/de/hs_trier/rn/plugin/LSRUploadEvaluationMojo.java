package de.hs_trier.rn.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "evaluate-lsr-uploads", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class LSRUploadEvaluationMojo extends AbstractUploadEvaluationMojo
{
    @Parameter(defaultValue = "${basedir}/../resources/${semester}/lsr-tasks", property = "inputFolder")
    private String inputFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/uploads", property = "uploadsFolder")
    private String uploadsFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/lsr-evaluation", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "lsr_solution_data", property = "tasksAndSolutionsFileName")
    private String inputFileName;

    @Parameter(defaultValue = "lsr_evaluation_data", property = "tasksAndUploadsFileName")
    private String outputFileName;

    @Override
    protected String getIdentifier()
    {
        return "LSR";
    }

    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }

    @Override
    protected String getUploadsFolder()
    {
        return uploadsFolder;
    }

    @Override
    protected int getNumberOfColumnsInInputFile()
    {
        return Constants.LSR_O_COL_NUM_S;
    }

    @Override
    protected int[] getSolutionColumns()
    {
        return new int[]{Constants.LSR_O_COL_SOL};
    }

    @Override
    protected String getInputFileName()
    {
        return inputFileName;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getOutputFileName()
    {
        return outputFileName;
    }

    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getLSRInstance();
    }
    
    @Override
    protected void processFile(File f, int index) throws IOException
    {
        data[index][getNumberOfColumnsInInputFile()] = MiscUtils.readStreamIntoLine(new FileInputStream(f));
    }

}
