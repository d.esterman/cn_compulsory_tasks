/*
 * CRCExerciseActivatorMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.processor.CRCExerciseActivatorGenerator;
import de.hs_trier.rn.processor.IGenerator;

@Mojo(name = "generate-crc-exercise-activator", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class CRCExerciseActivatorMojo extends AbstractExerciseActivatorMojo
{
    @Parameter(defaultValue = "${project.build.directory}/generated-sources/activator", property = "sourceFolder")
    private String sourceFolder;

    @Parameter(defaultValue = "${semester}", required = true, property = "semester")
    private String semester;

    @Parameter(defaultValue = "de.hs_trier.rn.gui.crc", property = "packageName")
    private String packageName;

    @Parameter(defaultValue = "CRCExerciseActivator", property = "className")
    private String className;
    
    @Override
    protected IGenerator getGenerator()
    {
        return new CRCExerciseActivatorGenerator();
    }

    @Override
    protected String getIdentifier()
    {
        return "CRC";
    }

    @Override
    protected String getSourceFolder()
    {
        return sourceFolder;
    }

    @Override
    protected String getSemester()
    {
        return semester;
    }

    @Override
    protected String getPackageName()
    {
        return packageName;
    }

    @Override
    protected String getClassName()
    {
        return className;
    }

}
