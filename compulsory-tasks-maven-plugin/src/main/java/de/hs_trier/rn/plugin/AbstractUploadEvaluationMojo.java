package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.Constants.MAT_NR_LEN;
import static de.hs_trier.rn.utils.MiscUtils.toExcelColumnName;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;
import de.hs_trier.rn.utils.csv.TableWriter;
import de.hs_trier.rn.utils.csv.XSLXReader;

/**
 * Liest alle eingesendeten Lösungen ein, aggregiert diese in eine Datei mit
 * Musterlösungen und Vergleichen mit diesen.
 */
public abstract class AbstractUploadEvaluationMojo extends AbstractMojo
{
    protected String[][] data;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            int numberOfColumns = getNumberOfColumnsInInputFile();
            String headers[] = getOutputHeader();
            int[] solutionColumns = getSolutionColumns();
            for (int i = 0; i < getUploadFolders().length; i++)
            {
                int numberOfAdditionalColumns = solutionColumns.length > 1 ? solutionColumns.length * 2 + 1 : 1;
                data = readData(numberOfAdditionalColumns);
                Map<String, Integer> matrikelNummern = new HashMap<>();
                for (int j = 0; j < data.length; j++)
                {
                    matrikelNummern.put(data[j][Constants.O_COL_M_NR], j);
                }
                for (final File fileEntry : getUploadFolders()[i].listFiles())
                {
                    final String matNr = fileEntry.getName().substring(0, MAT_NR_LEN);
                    Integer index = matrikelNummern.get(matNr);
                    if (index != null)
                    {
                        processFile(fileEntry, index);
                        if (numberOfAdditionalColumns > 1)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.append("=AND(");
                            for (int j = 0; j < solutionColumns.length; j++)
                            {
                                String solutionCellName = toExcelColumnName(solutionColumns[j] + 1) + (index + 2);
                                String uploadedSolutionCellName = toExcelColumnName(numberOfColumns + j + 1) + (index + 2);
                                data[index][numberOfColumns + solutionColumns.length + j] = "=" + solutionCellName + "=" + uploadedSolutionCellName;
                                sb.append(toExcelColumnName(numberOfColumns + solutionColumns.length + j + 1) + (index + 2)).append(", ");
                            }
                            data[index][numberOfColumns + solutionColumns.length * 2] = sb.delete(sb.length() - 2, sb.length()).append(")").toString();
                        }
                    }
                    else
                    {
                        getLog().warn("Matrikelnummer: " + matNr + " nicht bekannt.");
                    }
                }
                Pattern p = Pattern.compile(".+_(\\d+)");
                Matcher m = p.matcher(getUploadFolders()[i].getName());
                String fileNameAddition = m.find() ? "_" + m.group(1) : Constants.EMPTYSTR;
                String filePath = MiscUtils.assembleFileName(getOutputFolder(), getOutputFileName(), fileNameAddition, Constants.XLSX_EXT);
                TableWriter.writeXLSX(filePath, headers, data);
                getLog().info(getIdentifier() + " task data aggregated with student uploads created at " + filePath);
            }
        }
        catch (Exception e)
        {
            throw new MojoExecutionException(Constants.EMPTYSTR, e);
        }
    }

    protected String[] getOutputHeader()
    {
        return getCSVHandler().getHeaderWithUploads();
    }

    protected abstract String getIdentifier();

    protected abstract String getInputFolder();

    protected File[] getUploadFolders()
    {
        ArrayList<File> toRet = new ArrayList<>();
        File[] files = new File(getUploadsFolder()).listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isDirectory() && files[i].getName().matches(getIdentifier().toLowerCase()+".*"))
            {
                toRet.add(files[i]);
            }
        }
        return toRet.toArray(new File[toRet.size()]);
    }

    protected abstract String getUploadsFolder();

    protected abstract int getNumberOfColumnsInInputFile();

    protected abstract int[] getSolutionColumns();

    protected abstract CSVHandler getCSVHandler();

    protected abstract String getInputFileName();

    protected abstract String getOutputFolder();

    protected abstract String getOutputFileName();
    
    protected String[][] readData(int numberOfAdditionalColumns) throws Exception
    {
        return XSLXReader.readXLSX(MiscUtils.assembleFileName(getInputFolder(), getInputFileName(), Constants.XLSX_EXT), getNumberOfColumnsInInputFile() + numberOfAdditionalColumns);
    }
    
    protected abstract void processFile(File f, int index) throws IOException;
}
