package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.MiscUtils.readLinesFromStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "evaluate-crc-uploads", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class CRCUploadEvaluationMojo extends AbstractUploadEvaluationMojo
{
    @Parameter(defaultValue = "${basedir}/../resources/${semester}/crc-tasks", property = "inputFolder")
    private String inputFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/uploads", property = "uploadsFolder")
    private String uploadsFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/crc-evaluation", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "crc_solution_data", property = "inputFileName")
    private String inputFileName;

    @Parameter(defaultValue = "crc_evaluation_data", property = "outputFileName")
    private String outputFileName;

    @Override
    protected String getIdentifier()
    {
        return "CRC";
    }

    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }
    
    @Override
    protected String getUploadsFolder()
    {
        return uploadsFolder;
    }

    @Override
    protected int getNumberOfColumnsInInputFile()
    {
        return Constants.CRC_O_COL_NUM_S;
    }

    @Override
    protected int[] getSolutionColumns()
    {
        return new int[]{Constants.O_COL_T_POL_1, Constants.O_COL_R_POL, Constants.O_COL_IS_ERR};
    }

    @Override
    protected String getInputFileName()
    {
        return inputFileName;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getOutputFileName()
    {
        return outputFileName;
    }

    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getCRCInstance();
    }

    @Override
    protected void processFile(File f, int index) throws IOException
    {
        String[] lines = readLinesFromStream(new FileInputStream(f));
        for (int j = 0; j < getSolutionColumns().length && j < lines.length; j++)
        {
            data[index][getNumberOfColumnsInInputFile() + j] = MiscUtils.removeLeadingZeros(lines[j]).trim();
        }
        data[index][getNumberOfColumnsInInputFile() + getSolutionColumns().length - 1] = "=" + MiscUtils.convertNumericStringToBool(data[index][getNumberOfColumnsInInputFile() + getSolutionColumns().length - 1]);
    }
}
