/*
 * CRCSolutionsRecalculatonMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "recalculate-crc-solutions", defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresProject = false)
public class CRCSolutionsRecalculatonMojo extends AbstractSolutionsRecalculatonMojo
{
    @Parameter(defaultValue = "${basedir}/../resources/${semester}/crc-tasks", property = "inputFolder")
    private String inputFolder;
    
    @Parameter(defaultValue = "${basedir}/../compulsory-tasks-gui-crc/target/pflichtabgabe_crc_rn_${semester}.jar", property = "jarPath")
    private String jarPath;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/crc-tasks", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "crc_data", property = "tasksFileName")
    private String tasksFileName;

    @Parameter(defaultValue = "crc_solution_data_recovery", property = "tasksAndSolutionsFileName")
    private String tasksAndSolutionsFileName;


    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }
    
    @Override
    protected String getJARPath()
    {
        return jarPath;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getTasksFileName()
    {
        return tasksFileName;
    }
    
    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getCRCInstance();
    }

    @Override
    protected String getTasksAndSolutionsFileName()
    {
        return tasksAndSolutionsFileName;
    }
    
    @Override
    protected int[] getElementIndices()
    {
        return new int[] {0, 1, 2, 6};
    }
    
    @Override
    protected int getNumberOfColumnsToRead()
    {
        return Constants.CRC_O_COL_NUM_S;
    }
}
