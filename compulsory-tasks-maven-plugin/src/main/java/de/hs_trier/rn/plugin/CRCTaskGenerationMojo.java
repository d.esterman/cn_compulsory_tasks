/*
 * CRCTaskGenerationMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import static de.hs_trier.rn.utils.Constants.O_COL_E_POL;
import static de.hs_trier.rn.utils.Constants.O_COL_G_POL;
import static de.hs_trier.rn.utils.Constants.O_COL_M_NR;
import static de.hs_trier.rn.utils.Constants.O_COL_M_POL_1;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.csv.CSVHandler;

@Mojo(name = "generate-crc-tasks", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class CRCTaskGenerationMojo extends AbstractTaskGenerationMojo
{
    @Parameter(defaultValue = "${semester}", required = true, property = "semester")
    private String semester;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}", property = "inputFolder")
    private String inputFolder;

    @Parameter(defaultValue = "${basedir}/../resources/${semester}/crc-tasks", property = "outputFolder")
    private String outputFolder;

    @Parameter(defaultValue = "crc_data", property = "tasksFileName")
    private String tasksFileName;

    @Parameter(defaultValue = "crc_solution_data", property = "tasksAndSolutionsFileName")
    private String tasksAndSolutionsFileName;

    @Override
    protected CSVHandler getCSVHandler()
    {
        return CSVHandler.getCRCInstance();
    }

    @Override
    protected String getIdentifier()
    {
        return "CRC";
    }

    @Override
    protected String getInputFolder()
    {
        return inputFolder;
    }

    @Override
    protected String getOutputFolder()
    {
        return outputFolder;
    }

    @Override
    protected String getTasksFileName()
    {
        return tasksFileName;
    }

    @Override
    protected int[] getElementIndicies()
    {
        return new int[]
        { O_COL_M_NR, O_COL_G_POL, O_COL_M_POL_1, O_COL_E_POL };
    }

    @Override
    protected String getTasksAndSolutionsFileName()
    {
        return tasksAndSolutionsFileName;
    }
}
