package de.hs_trier.rn.plugin.nativeloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

public class NativeLoader {

    private Set<File> libFiles;
    
    public NativeLoader() {
        libFiles = new HashSet<>();
    }

    public void loadLibrary(String library) {
        try {
            System.load(saveLibrary(library));
        } catch (IOException e) {
//            LOG.warn("Could not find library " + library +
//                    " as resource, trying fallback lookup through System.loadLibrary");
            System.loadLibrary(library);
        }
    }
    
    public Set<File> getLibFiles() {
        return libFiles;
    }


    private String getOSSpecificLibraryName(String library, boolean includePath) {
        String osArch = System.getProperty("os.arch");
        String osName = System.getProperty("os.name").toLowerCase();
        String name;
        String path;

        if (osName.startsWith("win")) {
            name = library + ".dll";
            if (osArch.equalsIgnoreCase("amd64")) {
                path = "windows/x86_64/";
            }
            else if (osArch.equalsIgnoreCase("x86")) {
                path = "windows/x86/";
            } else {
                throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
            }
        } else if (osName.startsWith("linux")) {
            name = "lib" + library + ".so";
            if (osArch.equalsIgnoreCase("amd64")) {
                path = "linux/amd64/";
            } else if (osArch.equalsIgnoreCase("i386")) {
                path = "linux/i386/";
            } else {
                throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
            }
        } else {
            throw new UnsupportedOperationException("Platform " + osName + ":" + osArch + " not supported");
        }

        return includePath ? path + name : name;
    }

    private String saveLibrary(String library) throws IOException {
        InputStream in = null;
        OutputStream out = null;

        try {
            String libraryName = getOSSpecificLibraryName(library, true);
            in = this.getClass().getClassLoader().getResourceAsStream("lib/" + libraryName);
            String tmpDirName = System.getProperty("java.io.tmpdir");
            File tmpDir = new File(tmpDirName);
            if (!tmpDir.exists()) {
                tmpDir.mkdir();
            }
            File file = File.createTempFile(library + "-", ".tmp", tmpDir);

            out = new FileOutputStream(file);

            int cnt;
            byte buf[] = new byte[16 * 1024];
            // copy until done.
            while ((cnt = in.read(buf)) >= 1) {
                out.write(buf, 0, cnt);
            }
//            LOG.info("Saved libfile: " + file.getAbsoluteFile());
            libFiles.add(file);
            return file.getAbsolutePath();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignore) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}