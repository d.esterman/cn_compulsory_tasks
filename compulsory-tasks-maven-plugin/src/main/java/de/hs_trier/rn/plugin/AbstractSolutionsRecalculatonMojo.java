/*
 * AbstractSolutionsRecalculationMojo
 * 
 * Created by: Danylo Esterman (estermad@hochschule-trier.de)
 * 
 */
package de.hs_trier.rn.plugin;

import java.io.File;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import de.hs_trier.rn.utils.Constants;
import de.hs_trier.rn.utils.MiscUtils;
import de.hs_trier.rn.utils.csv.CSVHandler;
import de.hs_trier.rn.utils.csv.CSVReader;

public abstract class AbstractSolutionsRecalculatonMojo extends AbstractMojo
{
    @Parameter(defaultValue = "true", property = "readFromJAR")
    protected boolean readFromJAR;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        if (readFromJAR)
        {
            try (ZipFile jar = new ZipFile(new File(getJARPath())))
            {
                String fileName = MiscUtils.assembleFileName(null, getTasksFileName(), Constants.CSV_EXT);
                ZipEntry taskEntry = jar.getEntry(fileName);
                if (taskEntry == null)
                    throw new MojoExecutionException("Task file not found (" + getJARPath() + "!" + fileName);
                getCSVHandler().rewriteSolutions(MiscUtils.assembleFileName(getOutputFolder(), getTasksAndSolutionsFileName(), Constants.XLSX_EXT), CSVReader.readCSV(jar.getInputStream(taskEntry), getElementIndices(), false, getNumberOfColumnsToRead()));
            }
            catch (Exception e)
            {
                throw new MojoExecutionException(Constants.EMPTYSTR, e);
            }
        }
        else
        {
            try
            {
                getCSVHandler().rewriteSolutions(getOutputFolder() + "/" + getTasksAndSolutionsFileName(), CSVReader.readCSV(getInputFolder() + "/" + getTasksFileName(), getElementIndices(), getNumberOfColumnsToRead()));
            }
            catch (Exception e)
            {
                throw new MojoExecutionException(Constants.EMPTYSTR, e);
            }
        }
    }

    protected abstract String getInputFolder();

    protected abstract String getJARPath();

    protected abstract String getTasksFileName();

    protected abstract String getOutputFolder();

    protected abstract CSVHandler getCSVHandler();

    protected abstract String getTasksAndSolutionsFileName();

    protected abstract int[] getElementIndices();

    protected abstract int getNumberOfColumnsToRead();
}
