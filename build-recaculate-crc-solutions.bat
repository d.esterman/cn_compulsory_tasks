@echo off
for %%X in (mvn.bat) do (set mvn=%%~$PATH:X)
if not defined mvn for %%X in (mvn.cmd) do (set mvn=%%~$PATH:X)
if not defined mvn goto fail

call mvn -DjarPath=compulsory-tasks-gui-crc\target\pflichtabgabe_crc_rn_${semester}.jar -DoutputFolder=resources\${semester}\crc-tasks de.hs-trier.rn:compulsory-tasks-maven-plugin:0.0.1-SNAPSHOT:recalculate-crc-solutions
pause
exit /b 0

:fail
echo mvn.bat not found on path
pause
exit /b 1