@echo off
for %%X in (mvn.bat) do (set mvn=%%~$PATH:X)
if not defined mvn for %%X in (mvn.cmd) do (set mvn=%%~$PATH:X)
if not defined mvn goto fail

call mvn -DjarPath=compulsory-tasks-gui-lsr\target\pflichtabgabe_lsr_rn_${semester}.jar -DoutputFolder=resources\${semester}\lsr-tasks de.hs-trier.rn:compulsory-tasks-maven-plugin:0.0.1-SNAPSHOT:recalculate-lsr-solutions
pause
exit /b 0

:fail
echo mvn.bat not found on path
pause
exit /b 1