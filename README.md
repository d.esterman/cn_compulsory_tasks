# Computer Networks compulsory tasks.
### About

This repository provides software projects for the assistance of course Computer Networks at University of Applied Sciences Trier supervised by Prof. Dr. Konstantin Knorr. The contained artifacts should help to generate compulsory tasks, to build GUI, which can be distributed to students, and to evaluate solutions uploaded by students.

### Prerequisites

To successfully use the provided software or to contribute to the repository the following requirements should be met:

  - [JDK] 1.7 or higher
  - [Git] / [EGit] / Any other git provider
  - [Apache Maven] 3.3.1 or higher / [M2E]
  - [Generate] and [add] ssh keys.
  - Check the correctness of [semester property]

### Setup

After the installation of JDK the environment variable `JAVA_HOME` should be set to the installation directory.

To get the repository it needs to be checked out by means of git using the following URL:

```
git@gitlab.com:d.esterman/figure_fingerprint_sensor.git
```
If you want to use сonsole maven and the batch files, you need to put its bin directory to the environment variable `PATH`.

### Contents

The repository contains of 7 maven projects and 1 resource directory:

- *.*: the parent project containing the global build configuration and settings inherited by the other projects.
- *compulsory-tasks-utils*: utilities for CSV, XLSX processing, computation of LSR and CRC
- *compulsory-tasks-maven-plugin*: the central interface which provides all the automation tasks as plugin goals: generate-crc-tasks, generate-lsr-tasks, generate-crc-exercise-activator, generate-lsr-exercise-activator, evaluate-crc-uploads, evaluate-lsr-uploads, evaluate-pcap-uploads, recalculate-crc-solutions, recalculate-lsr-solutions
- *compulsory-tasks-generator*: build project for generating crc and lsr-tasks
- *compulsory-tasks-gui-crc*: project to compile and package GUI artifact for the Cyclic Redundancy Check task
- *compulsory-tasks-gui-lsr*: project to compile and package GUI artifact for the Link-State Routing task
- *compulsory-tasks-evaluation*: project to automate the evaluation of solutions uploaded by students
- *compulsory-tasks-recalculation*: a project to create solution files by processing files without solutions (to use in emergency case or manual amendments)


   [git]: <https://git-scm.com/downloads>
   [egit]: <https://www.eclipse.org/downloads/>
   [m2e]: <https://www.eclipse.org/downloads/>
   [generate]: <https://gitlab.com/help/ssh/README>
   [add]: <https://gitlab.com/profile/keys>
   [jdk]: <http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>
   [apache maven]: <https://maven.apache.org/download.cgi>
   [semester property]: <https://gitlab.com/d.esterman/cn_compulsory_tasks/blob/master/pom.xml#L11>