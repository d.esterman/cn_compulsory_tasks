@echo off
for %%X in (mvn.bat) do (set mvn=%%~$PATH:X)
if not defined mvn for %%X in (mvn.cmd) do (set mvn=%%~$PATH:X)
if not defined mvn goto fail

call mvn install
pause
exit /b 0

:fail
echo mvn.bat not found on path
pause
exit /b 1